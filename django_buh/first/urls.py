from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenRefreshView

from first.views import MyTokenObtainPairView,  ThemeAPIView

router = DefaultRouter()

router.register(r'theme',ThemeAPIView, basename='theme')

urlpatterns = [
    path('token/', MyTokenObtainPairView.as_view(), name='token_create'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('bot/', include('bot.urls')),
    path('frames/', include('frames.urls')),
    path('book/', include('book.urls')),
    path('reports/', include('reports.urls')),
    path('school/', include('school.urls')),
    path('administration/', include('administration.urls')),
    path('grading/', include('grading.urls')),
    path('security/', include('security.urls')),
]


urlpatterns += router.urls