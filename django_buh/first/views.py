from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView

from first.classes import RetrieveUpdatePR, ListPermissionViewSetsPR
from first.models import ProxyUser
from first.serializer import MyTokenObtainPairSerializer,  ThemeSerializer


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class ThemeAPIView(ListPermissionViewSetsPR, RetrieveUpdatePR):
    """
        Возвращает  тему
    """

    permission_classes = [IsAuthenticated]
    serializer_class = ThemeSerializer

    def get_queryset(self, **kwargs):
        user = ProxyUser.objects.filter(id=self.request.user.id)

        return user
