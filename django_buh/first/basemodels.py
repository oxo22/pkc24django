from django.conf import settings
from django.db import models

""" 
Тут описаны базовые модели всех таблиц
"""


class Common(models.Model):
    create_date_time = models.DateTimeField(auto_now_add=True, editable=False, help_text='Дата создания')
    create_user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True,
                                    null=True, editable=False,
                                    on_delete=models.PROTECT, related_name="%(class)s_created", help_text='Кто создал')
    change_date_time = models.DateTimeField(auto_now=True, editable=False, help_text='Дата обновления')
    change_user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True,
                                    null=True, editable=False,
                                    on_delete=models.SET_NULL, related_name="%(class)s_modified", help_text='Кто обновил')
    in_archive = models.BooleanField(default=False, verbose_name='Архив', blank=True)

    class Meta:
        abstract = True
