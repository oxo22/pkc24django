from rest_framework import mixins, viewsets


class MixedPermission:
    """Миксин permissions для action"""

    def get_permissions(self):
        return [permission() for permission in self.permission_classes]


class ListPermissionViewSetsPR(mixins.ListModelMixin, MixedPermission, viewsets.GenericViewSet):
    """"""
    pass


class CreatePR(mixins.CreateModelMixin,
               MixedPermission,
               viewsets.GenericViewSet):
    """"""
    pass


class RetrieveUpdatePR(mixins.RetrieveModelMixin,
                       mixins.UpdateModelMixin,
                       MixedPermission,
                       viewsets.GenericViewSet):
    """"""
    pass


class CreateUpdateDestroyPR(mixins.CreateModelMixin,
                            mixins.UpdateModelMixin,
                            mixins.DestroyModelMixin,
                            MixedPermission,
                            viewsets.GenericViewSet):
    """"""
    pass



class CreateRetrieveUpdateDestroyPR(mixins.CreateModelMixin,
                                    mixins.RetrieveModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.DestroyModelMixin,
                                    MixedPermission,
                                    viewsets.GenericViewSet):
    """"""
    pass



class MixedPermissionViewSetPR(MixedPermission, viewsets.ViewSet):
    pass


class MixedPermissionGenericViewSetPR(MixedPermission, viewsets.GenericViewSet):

    pass
