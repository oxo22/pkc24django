from datetime import datetime
from django.core.management import BaseCommand
import requests
from rest_framework.utils import json



class Command(BaseCommand):
    help = 'Получает по api список дат контактов в блиэайшие 15 минут, и отправляет сообщения через бота на адреса' \
   'ответственных, при удачной отправке ставит пометку "True" в базе данных, исключая события из последующих рассылок '

    def handle(self, *args, **options):

        BOT_TOKEN = '001.1684895732.1058766141:753666709'

        response = requests.post('https://rio-crux.ru/api/token/',
                                 {'username':'bot_user', 'password':'hWvm296YFvLWXBGZrvGgJ6Q9ME'}) # get-запрос

        ACCESS_TOKEN = json.loads(response.text)['access']




        response = requests.get('https://rio-crux.ru/api/bot/get_interview_date_bot/?format=json',
                                headers={'Authorization': 'Bearer ' + ACCESS_TOKEN }) # get-запрос
        response = json.loads(response.text)

        events = []


        for elem in response['results']:
            event = {
                'myteam_account'    : elem['celebrated']['person']['myteam_account'],
                'celebrated_id'     : str(elem['celebrated']['id']),
                'interview_date'    : elem['interview_date'],
                'interview_id'      : str(elem['id']),
                'data':{
                    'id'            : str(elem['person']['id']),
                    'last_name'     : elem['person']['last_name'],
                    'first_name'    : elem['person']['first_name'],
                    'middle_name'   : elem['person']['middle_name'],
                    'phone'         : elem['person']['person_phone_frames_person'][0]['phone'],
                    'descr'         : elem['person']['candidate_state_frames_person'][0]['candidate_status']['descr'],
                    'note'          : elem['note']
                },
            }
            events.append(event)


        for key in events:
            if key['myteam_account']:
                date_time = datetime.strptime(key['interview_date'], '%Y-%m-%dT%H:%M')
                date = date_time.date().strftime( '%d.%m.%Y')
                time = date_time.time().strftime( '%H:%M')
                full_name = key['data']['last_name'] + ' ' + key['data']['first_name'] + ' ' + key['data']['middle_name']


                message = 'НАПОМИНАНИЕ!!!  \n ' + \
                      date + '\n' + \
                      ' В ' + time + ' назначена встреча с ' +  full_name + ' \n' +\
                      'Комментарий к событию: ' + '\n' +  (lambda x: x or ' ')(key['data']['note'])


                response = requests.get('https://myteam.mail.ru/bot/v1/messages/sendText?token=' +\
                                        BOT_TOKEN + '&chatId=' + key['myteam_account'] + '&text=' + message)

                if response.status_code == 200:
                    commit = {
                                'interview_date' : key['interview_date'] ,
                                'person_id': key['data']['id'],
                                'celebrated_id' : key['celebrated_id'],
                                'send_myteam' : 1,
                              }
                    response = requests.put(
                        'https://rio-crux.ru/api/bot/get_interview_date_bot/' + key['interview_id'] + '/', commit,
                            headers={'Authorization': 'Bearer ' + ACCESS_TOKEN })
