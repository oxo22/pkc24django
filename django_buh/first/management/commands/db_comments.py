from django.apps import apps
from django.core.management import BaseCommand
from django.db import connection
from frames.models.person import CHANGE_LIST

change_list = CHANGE_LIST


class Command(BaseCommand):
    help = 'Добавляет комментарии из help_text'

    def add_arguments(self, parser):
        parser.add_argument('app', type=str, help='Имя приложения')

    def handle(self, *args, **kwargs):
        app = kwargs['app']
        fields = {}
        # получаем модели приложения
        app_models = apps.get_app_config(app).get_models()
        data_from_mysql = {}
        # коннект к базе
        cursor = connection.cursor()
        # Список колонок которые нужно пропустить
        skip_lst = ['id']

        for item in app_models:
            sql_querry = 'DESCRIBE ' + item._meta.db_table

            cursor.execute(sql_querry)
            rows = cursor.fetchall()

            # заполнение данных (название столбца, тип данных, из мускуля)
            for row in rows:
                if row[0] not in skip_lst:
                    data_from_mysql.setdefault(row[0], row[1])

            options = item._meta

            # собираем словарь моделей
            model = {}
            for field in sorted(options.concrete_fields + options.many_to_many + options.fields):
                # Проверяем если в change_list - добавляем id
                if field.name in change_list:
                    field.name = field.name + '_id'
                # Собираем словарь {имя_поля:[комментарий, тип_данных]}
                # Если в skil-list-е - пропускаем
                if field.name not in skip_lst:
                    #Иначе присваиваем ключу с именем поля модели значением список из поля help_text
                    model[field.name] = [field.help_text]
                    #Есоли имя поля есть в данных с мускуля
                    if field.name in data_from_mysql:
                        #добавляем к списку  строку результат проверки наличия blank
                        model[field.name].append(
                            data_from_mysql[field.name] + (
                                ' NULL ' if field.blank else ' NOT NULL '
                            )
                        )
            #Собираем запрос
            sql_querry = ''
            for inst_model in model:
                table_name = item._meta.db_table
                sql_querry = sql_querry + 'ALTER TABLE {} MODIFY {} {} COMMENT \'{}\';'.format(table_name, inst_model,
                                                                                                model[inst_model][1],
                                                                                                model[inst_model][0])
            #Выполняем запрос
            cursor.execute(sql_querry)
