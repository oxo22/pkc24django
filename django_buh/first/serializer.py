from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from first.models import ProxyUser
from frames.models.person import Person


class IsUserHasPersonSerializer(serializers.ListSerializer):
    '''
        Фильтр юзеров по полю person, возвращает тех у кого поле person не null
    '''

    def to_representation(self, data):
        data = data.filter(person__isnull=False)
        return super().to_representation(data)


class PersonUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person
        fields = ['id', 'last_name', 'first_name', 'myteam_account']


class UserSerializer(serializers.ModelSerializer):
    '''
        Серилизатор пользователя
    '''

    person = PersonUserSerializer(read_only=True)

    class Meta:
        model = ProxyUser
        fields = ['id', 'person']
        list_serializer_class = IsUserHasPersonSerializer


class ChildrenSerializer(serializers.Serializer):
    '''
            Серилизатор Подменю
    '''
    id = serializers.CharField()
    title = serializers.CharField()
    parent__url = serializers.CharField()
    url = serializers.CharField()
    hint = serializers.CharField()


class MenuSerializer(serializers.Serializer):
    '''
        Серилизатор меню
    '''
    id  = serializers.CharField()
    title  = serializers.CharField()
    url  = serializers.CharField()
    hint = serializers.CharField()
    children = ChildrenSerializer(read_only=True, many=True)


class ThemeSerializer(serializers.ModelSerializer):
    '''
        Серилизатор темы
    '''

    class Meta:
        model = ProxyUser
        fields = ['id', 'background_image', 'background_toolbar_color', 'background_toolbar_flag', 'background_top_flag', 'background_left_flag',  ]




class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    '''
            Серилизатор токена
    '''
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        # Add extra responses here
        data['username'] = self.user.username
        data['id'] = self.user.id
        data['groups'] = self.user.groups.values_list('name', flat=True)
        return data

class ChangePasswordSerializer(serializers.Serializer):
    """
        Серилизатор для апи изменения пароля
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

