import math

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class MainPagination(PageNumberPagination):
    def get_paginated_response(self, data):
        return Response({
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'count': self.page.paginator.count,
            'count_pages': math.ceil(self.page.paginator.count / 15),
            'results': data
        })
