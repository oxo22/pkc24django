from django.contrib.auth.models import AbstractUser
from django.db import models

from book.models import PersonStatus
from frames.models.person import Person


class User(AbstractUser):
    """
    Users within the Django authentication system are represented by this
    model.

    Username and password are required. Other fields are optional.
    """
    person = models.OneToOneField(Person,
                                  on_delete=models.PROTECT,
                                  related_name='user_frames_person',
                                  null=True,
                                  verbose_name='Персона',
                                  help_text='Ссылка на таблицу doc_person',
                                  blank=True, )

    background_image = models.URLField(null=True, blank=True)

    background_toolbar_color = models.CharField(null=True,
                                                blank=True,
                                                max_length=50,
                                                default='#263238')

    background_toolbar_flag = models.BooleanField(default=True,
                                                  verbose_name='Флаг True/False',
                                                  blank=True)

    background_top_flag = models.BooleanField(default=True,
                                              verbose_name='Флаг True/False',
                                              blank=True)

    background_left_flag = models.BooleanField(default=True,
                                               verbose_name='Флаг True/False',
                                               blank=True)

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'


class ProxyUser(User):
    pass

    class Meta:
        app_label = 'auth'
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        proxy = True
