from rest_framework import permissions
from rest_framework.permissions import BasePermission, DjangoModelPermissions


class IsMemberGroup(DjangoModelPermissions):
    """Участик группы или админимтратор"""
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

#
# class IsAuthorEntry(BasePermission):
#     """Автор записи или админимтратор"""
#     def has_object_permission(self, request, view, obj):
#         return obj.author == request.user or obj.group.founder == request.user
#
#
# class IsAuthorCommentEntry(BasePermission):
#     """Автор комментария или админимтратор"""
#     def has_object_permission(self, request, view, obj):
#         return obj.author == request.user or obj.entry.group.founder == request.user
#
#
# class IsAuthorComment(BasePermission):
#     """Автор комментария или записи"""
#     def has_permission(self, request, view):
#         if request.method in permissions.SAFE_METHODS:
#             return True
#         return request.user and request.user.is_authenticated
#
#     def has_object_permission(self, request, view, obj):
#         return obj.user == request.user