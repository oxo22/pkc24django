from django.urls import path
from rest_framework.routers import DefaultRouter

from bot.views import InterviewBotModelView

router = DefaultRouter()

router.register(r'get_interview_date_bot', InterviewBotModelView, basename='interview_bot')
# router.register(r'test', TestBotModelView, basename='test')

#
urlpatterns = [
    # zpath('get_interview_date_bot', InterviewBotModelView.as_view()),
]

urlpatterns += router.urls
