from datetime import datetime, timedelta

from rest_framework.permissions import IsAuthenticated

from first.classes import ListPermissionViewSetsPR, RetrieveUpdatePR
from frames.models.interview import Interview
from frames.serializers.interview import InterviewSerializer


class InterviewBotModelView(ListPermissionViewSetsPR, RetrieveUpdatePR):
    '''
                Класс получения дат контактов персоны

                Возвращает список дат контактов персоны
    '''

    permission_classes = [IsAuthenticated]

    serializer_class = InterviewSerializer

    def get_queryset(self):
        date_start = datetime.now()
        date_stop = date_start + timedelta(minutes=15)
        queryset = Interview.objects.filter(interview_date__range=[date_start, date_stop], send_myteam=False)

        return queryset
