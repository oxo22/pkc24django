import pandas as pd

from datetime import datetime

from django.http import HttpResponse
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView


class MikrotikAPIView(APIView):
    """    Mikrotik.

    """
    permission_classes = [AllowAny]

    @staticmethod
    def post(request):
        #Записывает данные в файл. ожидпет передачи
        #TODO убрать с записи пустые мак адреса
        date = datetime.now().strftime('%Y-%m-%d')
        time = datetime.now().strftime('%H:%M')

        text = request.query_params['body']

        clients = [[date, time] + client for client in [pair.split('*') for pair in text.split('_') if len(pair) > 0]]

        df = pd.DataFrame(clients)
        df.to_csv('tmp/sec.csv', index=False, header=False, mode='a')

        return HttpResponse(status=200)