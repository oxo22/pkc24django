import pandas as pd

from datetime import datetime

from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from security.serializer import MikrotikSerializer, MikrotikRSOSerializer
from security.service import MikrotikBase


class MikrotikFullReportsView(ListAPIView):
    """
        Отчет по подключениям к интернету на отделениях

    """

    permission_classes = [IsAuthenticated]
    serializer_class = MikrotikSerializer

    def get_queryset(self, **kwargs):
        """  Возвращает данные из файла.

        """
        date = self.request.GET.get('date')
        office = self.request.GET.get('office')
        mac = self.request.GET.get('mac')
        rso_fio = self.request.GET.get('rso')
        detailed = self.request.GET.get('detailed')
        mounth = self.request.GET.get('mounth')

        # Определяем период с какого файла брать данные
        period = '-'.join(date.split('-')[:2])
        period_now = datetime.now().strftime('%Y-%m')
        path = MikrotikBase.get_root_file_csv(period, period_now)

        # выбераем список РСО
        rso_path = 'media/security/rso.csv'
        rso = pd.read_csv(rso_path, index_col=False)

        #соеденяем 2 файла
        clients = pd.read_csv(path, index_col=False)
        clients = clients.merge(rso, how='left', on='mac')

        #применяем фильтра
        if mounth == 'true':
            clients = clients.query('date.str.contains(@period)')
        if date and mounth == 'false':
            clients = clients.query('date == @date')
        if mac:
            clients = clients.query('mac == @mac')
        if office:
            clients = clients.query('office == @office ')
        if rso_fio:
            clients = clients.query('fio == @rso_fio ')
        if detailed == 'true':
            clients = clients.values.tolist()
            json_data_list = [MikrotikBase.mikrotik_full_data_convert(element) for element in clients]
        else:
            clients = clients.groupby(['date', 'office', 'fio']).size().reset_index(name='counts')
            clients = clients.values.tolist()
            json_data_list = [MikrotikBase.mikrotik_data_convert(element) for element in clients]
        return json_data_list


class MikrotikRSOView(ListAPIView):
    """   Возвращает справочник РСО

    """

    permission_classes = [IsAuthenticated]
    serializer_class = MikrotikRSOSerializer

    def get_queryset(self, **kwargs):
        """  Возвращает данные по РСО из файла.

        """
        rso_path = 'media/security/rso.csv'

        rso = pd.read_csv(rso_path, index_col=False)

        rso = rso['fio']
        rso = rso.drop_duplicates(keep='first')

        json_data_list = [MikrotikBase.rso_data_convert(element) for element in rso]

        return json_data_list
