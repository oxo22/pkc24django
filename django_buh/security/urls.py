from django.urls import path
from rest_framework.routers import DefaultRouter

from security.views.mikrotikAPIView import MikrotikAPIView
from security.views.mikrotikReports import MikrotikRSOView, MikrotikFullReportsView

router = DefaultRouter()

urlpatterns = [
    path('mikrotik/', MikrotikAPIView.as_view(), name='mikrotik'),
    path('mikrotik_full/', MikrotikFullReportsView.as_view(), name='mikrotik_full'),
    path('mikrotik_rso/', MikrotikRSOView.as_view(), name='mikrotik_rso'),

]


urlpatterns += router.urls