from datetime import timedelta
def min_to_time(min):
    sec = timedelta(minutes=min)
    time = str(sec)
    return  time

class MikrotikBase():

    @staticmethod
    def mikrotik_full_data_convert(data):
        # Формируем словарь. Для полного запроса
        res = {
            'date': data[0],
            'time': data[1],
            'office': data[2],
            'fio': data[5],

        }

        return res

    @staticmethod
    def mikrotik_data_convert(data):
        # Формируем словарь. Для полного запроса
        res = {
            'date': data[0],
            'time': min_to_time(data[3]),
            'office': data[1],
            'fio': data[2],

        }

        return res

    @staticmethod
    def rso_data_convert(data):
        # Формируем словарь. Для РСО
        res = {
            'fio': data,
        }

        return res

    @staticmethod
    def get_root_file_csv(period, period_now):
        # выбрать файл с данными в зависимости от даты
        if period == period_now:
            path = 'tmp/sec.csv'
        else:
            path = 'media/security/' + period + '.csv'
        return path
