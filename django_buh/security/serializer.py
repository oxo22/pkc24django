from rest_framework import serializers


class MikrotikSerializer(serializers.Serializer):

    date = serializers.CharField()
    time = serializers.CharField()
    office = serializers.CharField()
    fio = serializers.CharField()


class MikrotikRSOSerializer(serializers.Serializer):
    fio = serializers.CharField()
