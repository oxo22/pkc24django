from django_buh import settings
from first.basemodels import Common
from django.db import models

from frames.models.person import Person


class Interview(Common):

    id = models.AutoField(primary_key=True)

    interview_date = models.DateTimeField(blank=True,
                                          verbose_name='Дата и время собседования',
                                          help_text='Дата и время обеседования')

    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE,
                               related_name='interview_frames_person',
                               null=True,
                               verbose_name='Собеседование',
                               help_text='Ссылка на таблицу doc_person')

    note = models.TextField(blank=True,
                            null=True,
                            verbose_name='Коммент',
                            help_text='Коммент')

    note_supervisor = models.TextField(blank=True,
                                       null=True,
                                       verbose_name='Коммент руководителя',
                                       help_text='Коммент руководителя')

    note_staff = models.TextField(blank=True,
                                  null=True,
                                  verbose_name='Коммент сотрудника',
                                  help_text='Коммент сотрудника')

    celebrated = models.ForeignKey(settings.AUTH_USER_MODEL,
                                   on_delete=models.PROTECT,
                                   related_name='interview_first_user',
                                   null=True,
                                   blank=True,
                                   verbose_name='Пользователь системы',
                                   help_text='Ссылка на таблицу first_user')

    send_myteam = models.BooleanField(default=False,
                                      verbose_name='Отправлено в myTeam',
                                      blank=True,)

    def __str__(self):
        return 'дата, время: %s - Персона: %s' % (
            self.interview_date, self.person.get_full_name)

    class Meta:
        db_table = 'tech_interview'
        verbose_name = 'Cобседование'
        verbose_name_plural = 'Собседования'
        ordering = ['-id']
