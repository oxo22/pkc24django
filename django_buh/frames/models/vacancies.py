from django.db import models

from book.models import VacancyStatus, TypeOfEmployment, ExperienceCandidate, Department, Priority, City, Office, \
    ReasonForOpening
from django_buh import settings
from first.basemodels import Common


class Vacancies(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Название вакансии',
                             help_text='Название вакансии')

    status = models.ForeignKey(VacancyStatus,
                               on_delete=models.PROTECT,
                               related_name='vacancies_book_vacancy_status',
                               null=True,
                               verbose_name='статус',
                               help_text='Ссылка на таблицу book_vacancy_status')

    type_of_employment = models.ForeignKey(TypeOfEmployment,
                                           on_delete=models.PROTECT,
                                           related_name='vacancies_book_type_of_employment',
                                           null=True,
                                           verbose_name='Вид занятости',
                                           help_text='Ссылка на таблицу book_type_of_employment')

    experience_candidate = models.ForeignKey(ExperienceCandidate,
                                             on_delete=models.PROTECT,
                                             related_name='vacancies_book_experience_candidate',
                                             null=True,
                                             verbose_name='статус',
                                             help_text='Ссылка на таблицу book_experience_candidate')

    department = models.ForeignKey(Department,
                                   on_delete=models.PROTECT,
                                   related_name='vacancies_book_department',
                                   null=True,
                                   blank=True,
                                   verbose_name='Департамент',
                                   help_text='Ссылка на таблицу book_department')

    office = models.ForeignKey(Office,
                               on_delete=models.PROTECT,
                               related_name='vacancies_book_office',
                               null=True,
                               blank=True,
                               verbose_name='Отделения',
                               help_text='Ссылка на таблицу book_office')

    opening_date = models.DateField(blank=True,
                                    null=True,
                                    verbose_name='Дата открытия',
                                    help_text='Дата открытия')

    closing_date = models.DateField(blank=True,
                                    null=True,
                                    verbose_name='Дата закрытия',
                                    help_text='Дата закрытия')

    min_the_salary = models.DecimalField(max_digits=19,
                                         decimal_places=2,
                                         blank=True,
                                         verbose_name='Мин. зарплата',
                                         help_text='Мин. зарплата')

    max_the_salary = models.DecimalField(max_digits=19,
                                         decimal_places=2,
                                         blank=True,
                                         verbose_name='Макс. зарплата',
                                         help_text='Мин. зарплата')

    note = models.CharField(max_length=600,
                            blank=True,
                            null=True,
                            verbose_name='Коммент',
                            help_text='Коммент')

    celebrated = models.ForeignKey(settings.AUTH_USER_MODEL,
                                   on_delete=models.PROTECT,
                                   related_name='vacancies_first_user',
                                   null=True,
                                   blank=True,
                                   verbose_name='Пользователь системы',
                                   help_text='Ссылка на таблицу first_user')

    priority = models.ForeignKey(Priority,
                                 on_delete=models.PROTECT,
                                 related_name='vacancies_book_priority',
                                 null=True,
                                 blank=True,
                                 verbose_name='Приоритет',
                                 help_text='Ссылка на таблицу book_priority')

    city = models.ForeignKey(City,
                             on_delete=models.PROTECT,
                             related_name='vacancies_book_city',
                             null=True,
                             blank=True,
                             verbose_name='Город',
                             help_text='Ссылка на таблицу book_city')

    reason_for_opening = models.ForeignKey(ReasonForOpening,
                                           on_delete=models.PROTECT,
                                           related_name='vacancies_book_reason_for_opening',
                                           null=True,
                                           blank=True,
                                           verbose_name='Причина открытия',
                                           help_text='Ссылка на таблицу book_reason_for_opening')

    def __str__(self):
        return 'id: %s : %s, %s' % (
            self.id, self.descr, self.status)

    class Meta:
        db_table = 'doc_vacancies'
        verbose_name = 'Вакансия'
        verbose_name_plural = 'Вакансии'
