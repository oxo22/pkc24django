import os

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from django_buh import settings
from first.basemodels import Common
from book.models import Country, City, RelationDegree, Gender, PersonStatus, CandidateStatus, ExperienceCandidate, \
    Position, WhereFindOut, ReasonForDismissal, Office
from frames.models.vacancies import Vacancies

CHANGE_LIST = [
    'formal_city',
    'fact_city',
    'fact_country',
    'formal_country',
    'create_user',
    'change_user',
    'user',
    'relation_degree',
    'passport',
    'gender',
    'person',
    'person_status',
    'bush',
    'city',
    'status',
    'type_of_employment',
    'experience_candidate',
    'department',
    'vacancies',
    'celebrated',
    'priority',
    'office',
    'candidate_status',
    'candidate_experience',
    'where_find_out',
    'position',
    'person',
    'reason_for_opening',

]


class Person(Common):
    id = models.AutoField(primary_key=True)

    last_name = models.CharField(max_length=255,
                                 blank=False,
                                 verbose_name='Фамилия',
                                 help_text='Фамилия')

    first_name = models.CharField(max_length=255,
                                  blank=False,
                                  verbose_name='Имя',
                                  help_text='Имя')

    middle_name = models.CharField(max_length=255,
                                   blank=False,
                                   verbose_name='Отчество',
                                   help_text='Отчество')

    gender = models.ForeignKey(Gender,
                               on_delete=models.PROTECT,
                               related_name='person_book_gender',
                               null=True,
                               blank=True,
                               verbose_name='Пол',
                               help_text='Ссылка на таблицу book_gender')

    birthday = models.DateField(blank=True,
                                null=True,
                                verbose_name='День рождени',
                                help_text='День рождения')

    age = models.IntegerField(blank=True,
                              null=True,
                              verbose_name='Возраст',
                              help_text='Возраст')

    haired_date = models.DateField(blank=True,
                                   null=True,
                                   verbose_name='Дата приянтия на работу',
                                   help_text='Дата приянтия на работу')

    fired_date = models.DateField(blank=True,
                                  null=True,
                                  verbose_name='Дата увольнения',
                                  help_text='Дата увольнения')

    tin = models.CharField(max_length=255,
                           blank=True,
                           null=True,
                           verbose_name='ИНН',
                           help_text='ИНН')

    wage_amount = models.DecimalField(max_digits=19,
                                      decimal_places=2,
                                      blank=True,
                                      null=True,
                                      verbose_name='Желаемая зарплата',
                                      help_text='Желаемая зарплата')

    formal_country = models.ForeignKey(Country,
                                       on_delete=models.PROTECT,
                                       related_name='person_book_formal_country',
                                       null=True,
                                       verbose_name='Страна прописки',
                                       blank=True,
                                       help_text='Ссылка на таблицу book_country')

    formal_city = models.ForeignKey(City,
                                    on_delete=models.PROTECT,
                                    related_name='person_book_formal_city',
                                    null=True,
                                    blank=True,
                                    verbose_name='Город прописки',
                                    help_text='Ссылка на таблицу book_city')

    formal_address = models.CharField(max_length=255,
                                      blank=True,
                                      verbose_name='Прописка. Адрес: кв/ул дом квартира',
                                      help_text='Прописка. Адрес: кв/ул дом квартира',
                                      null=True, )

    fact_country = models.ForeignKey(Country,
                                     on_delete=models.PROTECT,
                                     related_name='person_book_fact_country',
                                     null=True,
                                     verbose_name='Страна проживания',
                                     help_text='Ссылка на таблицу book_country',
                                     blank=True, )

    fact_city = models.ForeignKey(City,
                                  on_delete=models.PROTECT,
                                  related_name='person_book_fact_city',
                                  null=True,
                                  blank=True,
                                  verbose_name='Город проживания',
                                  help_text='Ссылка на таблицу book_city')

    fact_address = models.CharField(max_length=255,
                                    blank=True,
                                    verbose_name='Проживание. Адрес: кв/ул дом квартира',
                                    help_text='Проживание. Адрес: кв/ул дом квартира',
                                    null=True)

    note = models.TextField(blank=True,
                            null=True,
                            verbose_name='Коммент',
                            help_text='Коммент')

    vacancies = models.ForeignKey(Vacancies,
                                  on_delete=models.PROTECT,
                                  related_name='person_doc_vacancies',
                                  null=True,
                                  blank=True,
                                  verbose_name='Вакансия',
                                  help_text='Ссылка на таблицу doc_vacancies')

    person_status = models.ForeignKey(PersonStatus,
                                      on_delete=models.PROTECT,
                                      related_name='person_book_status',
                                      blank=False, verbose_name='Статус персоны',
                                      null=True,
                                      help_text='Ссылка на таблицу book_person_status')
    candidate_experience = models.ForeignKey(ExperienceCandidate,
                                             on_delete=models.PROTECT,
                                             null=True,
                                             blank=True,
                                             related_name='person_experience_candidate',
                                             verbose_name='Опыт кандидата',
                                             help_text='Ссылка на таблицу book_experience_candidate')

    position = models.ForeignKey(Position,
                                 on_delete=models.PROTECT, null=True, blank=True,
                                 related_name='person_book_position',
                                 verbose_name='Опыт кандидата',
                                 help_text='Ссылка на таблицу book_position')

    where_find_out = models.ForeignKey(WhereFindOut,
                                       on_delete=models.PROTECT,
                                       blank=True,
                                       null=True,
                                       related_name='person_book_where_find_out',
                                       verbose_name='Откуда узнал о вакансии',
                                       help_text='Ссылка на таблицу book_where_find_out')

    date_recruitment = models.DateField(blank=True,
                                        null=True,
                                        verbose_name='Дата приема на работу',
                                        help_text='Дата приема на работу')

    date_discharge = models.DateField(blank=True,
                                      null=True,
                                      verbose_name='Дата увольнения',
                                      help_text='Дата увольнения')

    myteam_account = models.EmailField(max_length=255,
                                       blank=True,
                                       null=True,
                                       verbose_name='Аккакнт в MyTeam',
                                       help_text='Аккаунт в MyTeam')

    celebrated = models.ForeignKey(settings.AUTH_USER_MODEL,
                                   on_delete=models.PROTECT,
                                   related_name='person_first_user',
                                   null=True,
                                   blank=True,
                                   verbose_name='Пользователь системы',
                                   help_text='Ссылка на таблицу first_user')

    reason_for_dismissal = models.ForeignKey(ReasonForDismissal,
                                             on_delete=models.PROTECT,
                                             blank=True,
                                             null=True,
                                             related_name='person_book_reason_for_dismissal',
                                             verbose_name='Причина увольнения',
                                             help_text='Причина увольнения')

    note_of_dismissal = models.TextField(blank=True,
                                         null=True,
                                         verbose_name='Коммент по увольнению',
                                         help_text='Коммент по увольнению')

    def __str__(self):
        return 'id: %s - %s %s %s Статус: %s' % (
            self.id, self.last_name, self.first_name, self.middle_name, self.person_status)

    class Meta:
        db_table = 'doc_person'
        verbose_name = 'Персона'
        verbose_name_plural = 'Персоны'
        ordering = ['-id']

    def get_full_name(self):
        return '%s %s %s' % (self.last_name, self.first_name, self.middle_name)


class PassportData(Common):
    id = models.AutoField(primary_key=True)

    fact_country = models.ForeignKey(Country,
                                     on_delete=models.PROTECT,
                                     related_name='passport_data_book_fact_country',
                                     null=False,
                                     verbose_name='Страна выдачи',
                                     help_text='Страна выдачи.Ссылка на таблицу book_country')

    serial_number = models.CharField(max_length=255,
                                     blank=False,
                                     verbose_name='Серия, №',
                                     help_text='серия, номер')

    date_of_issue = models.DateField(blank=True,
                                     verbose_name='Дата выдачи',
                                     help_text='Дата выдачи')

    issued_by = models.CharField(max_length=255,
                                 blank=False,
                                 verbose_name='Кем выдан',
                                 help_text='Кем выдан')

    passport = models.ForeignKey(Person,
                                 on_delete=models.CASCADE,
                                 related_name='passport',
                                 null=True,
                                 verbose_name='Паспорт',
                                 help_text='Ссылка на таблицу doc_person')

    def __str__(self):
        return 'id: %s : %s, %s,  %s' % (
            self.id, self.fact_country, self.serial_number, self.passport)

    class Meta:
        db_table = 'doc_passport_data'
        verbose_name = 'Паспортные данные'
        verbose_name_plural = 'Паспортные данные'


class FamilyComposition(Common):
    id = models.AutoField(primary_key=True)

    relation_degree = models.ForeignKey(RelationDegree,
                                        on_delete=models.PROTECT,
                                        related_name='family_composition_book_relation_degree',
                                        null=True,
                                        verbose_name='Степень родства',
                                        help_text='Ссылка на таблицу book_relation_degree')

    year_of_birth = models.IntegerField(blank=False,
                                        verbose_name='Год рождения',
                                        help_text='Год рождения')

    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE,
                               related_name='family_composition_frames_person',
                               null=True,
                               verbose_name='Персона',
                               help_text='Ссылка на таблицу doc_person')

    def __str__(self):
        return 'id: %s : %s - %s %s года рождения ' % (
            self.id, self.person.get_full_name(), self.relation_degree, self.year_of_birth)

    class Meta:
        db_table = 'doc_family_composition'
        verbose_name = 'Семейное положение'
        verbose_name_plural = 'Семейное положение'


class EducationalInstitution(Common):
    id = models.AutoField(primary_key=True)

    educational_institution = models.CharField(max_length=255,
                                               blank=False,
                                               verbose_name='Учебное заведение',
                                               help_text='Учебное заведение')

    year_of_issue = models.IntegerField(blank=False,
                                        verbose_name='Год выпуска',
                                        help_text='Год выпуска')

    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE,
                               related_name='educational_institution_frames_person',
                               null=True,
                               verbose_name='Персона',
                               help_text='Ссылка на таблицу doc_person')

    def __str__(self):
        return self.educational_institution

    class Meta:
        db_table = 'doc_educational_institution'
        verbose_name = 'Образовательное учреждение'
        verbose_name_plural = 'Образовательные учреждения'


STATUS = 1


class CandidateState(Common):
    id = models.AutoField(primary_key=True)

    passed = models.BooleanField(null=True,
                                 verbose_name='Прошол не прошол',
                                 blank=True)

    candidate_status = models.ForeignKey(CandidateStatus,
                                         on_delete=models.PROTECT,
                                         blank=True,
                                         null=True,
                                         related_name='candidate_state_book_candidate_status',
                                         verbose_name='Статус кандидата',
                                         help_text='Ссылка на таблицу book_candidate_status',
                                         default=STATUS)

    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE,
                               related_name='candidate_state_frames_person',
                               null=True,
                               verbose_name='Персона',
                               help_text='Ссылка на таблицу doc_person')

    start_date = models.DateField(auto_now_add=True,
                                  editable=False,
                                  verbose_name='Дата начала',
                                  help_text='Дата начала')

    def __str__(self):
        return self.person.get_full_name()

    # @receiver(pre_save)
    # def create_drive(instance, **kwargs):
    #
    #     print(instance.passed)

    @receiver(post_save, sender=Person)
    def create_drive(sender, instance, created, **kwargs):
        if created:
            CandidateState.objects.create(person=instance)

    class Meta:
        db_table = 'doc_candidate_state'
        verbose_name = 'Соттояние кандидата'
        verbose_name_plural = 'Соттояние кандидатов'
        ordering = ['-id']


class FilesPerson(Common):

    def get_upload_path(instance, filename):
        return os.path.join(str(instance.person.id) + "/files/" + filename)

    PHOTO = 'PH'
    RESUME = 'RS'

    TYPE_FILES_CHOISES = [
        (PHOTO, 'Фото'),
        (RESUME, 'Резюме'),

    ]

    id = models.AutoField(primary_key=True)

    descr = models.CharField(max_length=2,
                             choices=TYPE_FILES_CHOISES,
                             null=True,
                             blank=True)

    file = models.FileField(upload_to=get_upload_path,
                            blank=True, )

    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE,
                               related_name='photo_person_frames_person',
                               null=True,
                               verbose_name='Резюме',
                               help_text='Ссылка на таблицу doc_person')

    def __str__(self):
        return 'id: %s - %s' % (self.id, self.person.get_full_name())

    class Meta:
        db_table = 'doc_files_person'
        verbose_name = 'Файлы'
        verbose_name_plural = 'Файлы'
        unique_together = ('descr', 'file', 'person')


class PersonPhone(Common):
    id = models.AutoField(primary_key=True)

    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE,
                               related_name='person_phone_frames_person',
                               null=True,
                               verbose_name='Персона',
                               help_text='Ссылка на таблицу doc_person')

    phone = models.CharField(max_length=255,
                             blank=True,
                             null=True,
                             verbose_name='Номер телефона',
                             help_text='Номер телефона')

    telegram = models.BooleanField(default=False,
                                   verbose_name='Телеграмм',
                                   blank=True,
                                   null=True)

    whats_app = models.BooleanField(default=False,
                                    verbose_name='Вацап',
                                    blank=True,
                                    null=True)

    viber = models.BooleanField(default=False,
                                verbose_name='Вайбер',
                                blank=True,
                                null=True)

    def __str__(self):
        return self.person.get_full_name()

    class Meta:
        db_table = 'doc_person_phone'
        verbose_name = 'Контактные намера персоны'
        verbose_name_plural = 'Контактные намера персон'


class ProposalPerson(Common):
    id = models.AutoField(primary_key=True)

    candidate = models.OneToOneField(Person,
                                  on_delete=models.CASCADE,
                                  related_name='proposal_person_frames_candidate',
                                  null=True,
                                  verbose_name='Персона',
                                  help_text='Ссылка на таблицу doc_person')

    proposal_person = models.ForeignKey(Person,
                                 on_delete=models.CASCADE,
                                 related_name='proposal_person_frames_proposal',
                                 null=True,
                                 verbose_name='Персона',
                                 help_text='Ссылка на таблицу doc_person')

    office = models.ForeignKey(Office,
                               on_delete=models.PROTECT,
                               related_name='proposal_person_book_office',
                               null=True,
                               blank=True,
                               verbose_name='Отделения',
                               help_text='Ссылка на таблицу book_office')

    class Meta:
        db_table = 'doc_proposal_person'
        verbose_name = 'Кто посоветовал кандидата'
        verbose_name_plural = 'Кто посоветовал кандидата'
