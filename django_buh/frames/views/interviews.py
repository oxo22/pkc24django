from first.classes import CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR
from first.permissions import IsMemberGroup
from frames.models.interview import Interview
from frames.serializers.interview import InterviewCalendarListSerializer

from frames.serializers.person import InterviewSerializer


class InterviewModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    '''
                Класс получения дат контактов персоны

                Возвращает список дат контактов персоны
    '''

    permission_classes = [IsMemberGroup]
    queryset = Interview.objects.all()
    serializer_class = InterviewSerializer


class InterviewCalendarList(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    '''
                    Класс получения дат контактов персон для календаря

                    Возвращает список дат контактов персон для календаря
        '''

    permission_classes = [IsMemberGroup]
    serializer_class = InterviewCalendarListSerializer
    pagination_class = None

    def get_queryset(self):
        # Если даты передаются в запросе
        if 'date' in self.request.GET:
            # собираем date_start и date_stop для BETWEEN
            date = self.request.GET['date']
            user_id = self.request.GET['user_id']
        query = Interview.objects.filter(interview_date__contains=date).filter(celebrated=user_id)

        return query
