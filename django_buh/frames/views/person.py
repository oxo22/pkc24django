import os
import shutil
from datetime import datetime, timedelta
from django.db.models import Q, Max
from rest_framework.generics import ListAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from first.classes import CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR, CreatePR
from first.pagination import MainPagination
from first.permissions import IsMemberGroup
from frames.models.person import Person, FilesPerson, PersonPhone, CandidateState, ProposalPerson
from frames.serializers.person import PersonSerializer, FilesSerializer, PersonPhoneSerializer, \
    PersonCandidateStateSerializer, PkcPersonSerializer, PkcPersonPhoneSerializer, PersonsStaffSerializer, \
    ProposalPersonSerializer


class PersonsModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
        Класс отображения всех персон
    """
    permission_classes = [IsMemberGroup]
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class PkcPersonsModelView(CreatePR):
    """
        Класс заполнения персоны с сайта PKC24.ru

    """
    permission_classes = [IsMemberGroup]
    queryset = Person.objects.all()
    serializer_class = PkcPersonSerializer


class PkcPersonPhoneModelView(CreatePR):
    """
        Класс заполнения телефонов персоны с сайта PKC24.ru

    """
    permission_classes = [IsMemberGroup]
    queryset = PersonPhone.objects.all()
    serializer_class = PkcPersonPhoneSerializer


class PersonPhoneModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
        Класс отображения всех телефонов персон
    """
    permission_classes = [IsMemberGroup]
    queryset = PersonPhone.objects.all()
    serializer_class = PersonPhoneSerializer


class PersonStateModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
        Класс отображения всех состояний персон
        принимает параметр
        person_id - id персоны
    """
    permission_classes = [IsMemberGroup]
    serializer_class = PersonCandidateStateSerializer

    def get_queryset(self):
        queryset = CandidateState.objects.all()
        person_id = self.request.query_params.get('person_id', None)
        if person_id is not None:
            queryset = CandidateState.objects.filter(person_id=person_id)
        return queryset


class PersonsListView(ListAPIView):
    """
        Класс отображения списка персон, поиска по персонам и фильтрации по персонам

        - поиск принимает параметры:
                                    person_status  - id статуса персоны                     (type int)
                                    start_date -  дата, американский формат                 (type date)
                                    stop_date - дата, американский формат                   (type date)
                                    position - id должность кандидата                       (type int)
                                    data - строка поискового запроса по Ф.И.О               (type str)
                                    phone - строка поискового запроса по телефону           (type str)
                                    city- id города                                         (type int)
                                    note - строка поискового запроса по номентарию          (type str)
                                    internship_status - признак запроса по Стажировке       (type int)
                                    status - id статуса кандидата                           (type int)
                                    get_probation_list - признак возврата списка значений в
                                    диапазоне 45 дней от текущей даты по полю "haired_date" (type boolean)
                                    passed - признак возврата списка значений поле "passed"
                                    которых равно "Null"                                    (type boolean)

        - сортировка по полю 'id' desc

        Возвращает список результатов                                                       (type list)
    """

    permission_classes = [IsAuthenticated]
    serializer_class = PersonSerializer

    def get_queryset(self):

        queryset = Person.objects.all()

        start_date = self.request.GET.get('start_date')
        stop_date = self.request.GET.get('stop_date')
        position = self.request.GET.get('position')
        data = self.request.GET.get('data')
        phone = self.request.GET.get('phone')
        city = self.request.GET.get('city')
        note = self.request.GET.get('note')
        status = self.request.GET.get('status')
        internship_status = self.request.GET.get('internship_status')
        person_status = self.request.GET.get('person_status')
        get_probation_list = self.request.GET.get('get_probation_list')
        passed = self.request.GET.get('passed')
        celebrated = self.request.GET.get('celebrated')

        object_list = queryset

        if get_probation_list:
            object_list = object_list.filter(haired_date__range=[
                (datetime.now() - timedelta(days=90)).strftime('%Y-%m-%d'),
                datetime.now()
            ])

        if internship_status:
            object_list = object_list.annotate(
                max_candidate_state=Max('candidate_state_frames_person__candidate_status'))
            object_list = object_list.filter(max_candidate_state=internship_status)

        if passed:
            object_list = object_list.annotate(
                max_candidate_state=Max('candidate_state_frames_person__candidate_status'))
            object_list = object_list.filter(max_candidate_state=status)
            object_list = object_list.filter(candidate_state_frames_person__passed__isnull=True)

        if person_status:
            object_list = object_list.filter(person_status_id=person_status)

        if celebrated:
            object_list = object_list.filter(celebrated_id=celebrated)

        if start_date and stop_date:
            if person_status == '2':
                object_list = object_list.filter(haired_date__range=[
                    datetime.strptime(start_date, '%Y-%m-%d'),
                    datetime.strptime(stop_date, '%Y-%m-%d')
                ])
            elif person_status == '3':
                object_list = object_list.filter(faired_date__range=[
                    datetime.strptime(start_date + ' 00:00', '%Y-%m-%d %H:%M'),
                    datetime.strptime(stop_date + ' 23:59', '%Y-%m-%d %H:%M')
                ])
            elif internship_status:
                object_list = object_list.filter(candidate_state_frames_person__start_date__range=[
                    datetime.strptime(start_date, '%Y-%m-%d'),
                    datetime.strptime(stop_date, '%Y-%m-%d')
                ])
            else:
                object_list = object_list.filter(create_date_time__range=[
                    datetime.strptime(start_date + ' 00:00', '%Y-%m-%d %H:%M'),
                    datetime.strptime(stop_date + ' 23:59', '%Y-%m-%d %H:%M')
                ])

        if position:
            object_list = object_list.filter(position=position)

        if data:
            dataList = data.split()
            for elem in dataList:
                object_list = object_list.filter(
                    Q(last_name__icontains=elem) |
                    Q(first_name__icontains=elem) |
                    Q(middle_name__icontains=elem)
                )
        if phone:
            object_list = object_list.filter(
                Q(person_phone_frames_person__phone__icontains=phone)
            )
        if city:
            object_list = object_list.filter(fact_city=city)
        if note:
            object_list = object_list.filter(note__icontains=note)

        if status:
            object_list = object_list.annotate(
                max_candidate_state=Max('candidate_state_frames_person__candidate_status'))
            object_list = object_list.filter(max_candidate_state=status)

        return object_list


class PersonsStaffListView(ListAPIView):
    """
        Класс отображения списка персон сотрудников

    """

    permission_classes = [IsAuthenticated]
    serializer_class = PersonsStaffSerializer

    def get_queryset(self):
        queryset = Person.objects.filter(person_status=2)
        return queryset


class PhotoUploadView(APIView):
    """
            Класс загрузки фото персоны

           - принимает параметры:
                                       file_obj - объект файла          (type ___)
                                       username - id персоны            (type int)
                                       file_type - тип файла            (type str)


            Создает в папке media папку с именем username, и в подпапку photo ложит
           передаваемый файл переименовывая его в my_photo.jpg

            При удачном создании файла, создает соответствующую запись в базе данных
    """

    parser_classes = (MultiPartParser,)
    serializer_classes = FilesSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request):
        # print(request.data)
        file_obj = request.data['file']
        username = str(request.data['person_id'])
        file_type = str(request.data['file_type'])

        os.makedirs('media/{}/photo'.format(username), exist_ok=True)
        try:
            with open('tmp/my_photo.jpg', 'wb+') as file_handler:
                for line in file_obj.chunks():
                    file_handler.write(line)
            src = 'tmp/my_photo.jpg'
            dst = '/code/media/' + username + '/photo/my_photo.jpg'
            shutil.copy(src, dst)
            os.remove(src)
        except:
            return Response(status=500)
        file_path = username + '/photo/my_photo.jpg'
        FilesPerson.objects.update_or_create(
            descr=file_type,
            file=file_path,
            person_id=username
        )

        return Response(status=201)


class ResumeUploadView(APIView):
    """
                Класс загрузки резюме персоны

               - принимает параметры:
                                           file_obj - объект файла          (type ___)
                                           username - id персоны            (type int)
                                           file_type - тип файла            (type str)


                Создает в папке media папку с именем username, и в подпапку resume ложит
               передаваемый файл с именем file_obj.name

                При удачном создании файла, создает соответствующую запись в базе данных
    """

    parser_classes = (MultiPartParser,)
    serializer_classes = FilesSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request):
        # username = request.user.username
        # print(request.data)
        file_obj = request.data['file']
        username = str(request.data['person_id'])
        file_type = str(request.data['file_type'])

        os.makedirs('media/{}/resume'.format(username), exist_ok=True)

        try:
            with open('tmp/' + file_obj.name, 'wb+') as file_handler:
                for line in file_obj.chunks():
                    file_handler.write(line)

            src = 'tmp/' + file_obj.name
            dst = '/code/media/' + username + '/resume/' + file_obj.name

            shutil.copy(src, dst)
            os.remove(src)
        except:
            return Response(status=500)

        file_path = username + '/resume/' + file_obj.name
        FilesPerson.objects.update_or_create(
            descr=file_type,
            file=file_path,
            person_id=username
        )

        return Response(status=201)


class GetPersonFileModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
                Класс получения файлов персоны

               - принимает параметры:
                                           person_id - id персоны            (type int)


                Возвращает список файлов персоны
    """

    permission_classes = [IsMemberGroup]
    serializer_class = FilesSerializer

    def get_queryset(self):
        queryset = FilesPerson.objects.all()
        person_id = self.request.query_params.get('person_id', None)
        if person_id is not None:
            queryset = FilesPerson.objects.filter(person_id=person_id)
        return queryset


class ProposalPersonView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
                Класс  персоны предложившей кандидата
    """

    permission_classes = [IsMemberGroup]
    queryset = ProposalPerson.objects.all()
    serializer_class = ProposalPersonSerializer

