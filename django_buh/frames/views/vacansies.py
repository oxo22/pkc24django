from django.db.models import Count, Q
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from book.models import City, Office, Department, VacancyStatus, Priority, ReasonForOpening
from first.classes import CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR
from first.models import ProxyUser
from first.permissions import IsMemberGroup
from frames.models.vacancies import Vacancies
from frames.serializers.vacancie import VacanciesSerializer, VacanciesListSerializer, VacanciesBookSerializer, \
    VacanciesNameSerializer, VacanciesSerachSerializer


class VacanciesModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    '''Возвращает список всех ваканлий'''
    permission_classes = [IsMemberGroup]
    queryset = Vacancies.objects.all()
    serializer_class = VacanciesSerializer


class VacanciesListView(ListAPIView):
    '''Возвращает список всех вакансий
       и количество кандидатов в каждой вакансии
       может принимать параметр 'q' для фильтрации
       Сортировка по статусу 'asc'
    '''
    permission_classes = [IsAuthenticated]
    serializer_class = VacanciesListSerializer

    def get_queryset(self):
        queryset = Vacancies.objects.values(
            'id',
            'descr',
            'city__descr',
            'office__item',
            'department__descr',
            'celebrated__person__last_name',
            'status__descr',
            'priority__descr',
            'reason_for_opening__descr'
        ).annotate(count_candidate=Count('person_doc_vacancies__vacancies')) \
            .order_by('status__id', '-opening_date', '-id')

        object_list = queryset

        vacancies_names = self.request.GET.get('vacancies_names')
        city = self.request.GET.get('city')
        office = self.request.GET.get('office')
        department = self.request.GET.get('department')
        celebrated = self.request.GET.get('celebrated')
        status = self.request.GET.get('status')
        priority = self.request.GET.get('priority')
        reason_for_opening = self.request.GET.get('reason_for_opening')
        start_date = self.request.GET.get('start_date')
        stop_date = self.request.GET.get('stop_date')
        date_type = self.request.GET.get('date_type')

        if vacancies_names:
            object_list = object_list.filter(
                Q(descr__icontains=vacancies_names))
        if city:
            object_list = object_list.filter(
                Q(city__id=city))
        if office:
            object_list = object_list.filter(
                Q(office__id=office))
        if celebrated:
            object_list = object_list.filter(
                Q(celebrated__person=celebrated))
        if department:
            object_list = object_list.filter(
                Q(department__id=department))
        if status:
            object_list = object_list.filter(
                Q(status__id=status))
        if priority:
            object_list = object_list.filter(
                Q(priority__id=priority))
        if reason_for_opening:
            object_list = object_list.filter(
                Q(reason_for_opening=reason_for_opening))
        if start_date and stop_date:
            if date_type == 'open':
                object_list = object_list.filter(
                    Q(opening_date__range=[start_date, stop_date])
                )
            else:
                object_list = object_list.filter(
                    Q(closing_date__range=[start_date, stop_date])
                )
        return object_list


class VacanciesNameListView(ListAPIView):
    '''
        Возвращает список уникальных имен всех вакансий отсортированных по алфавиту
    '''
    permission_classes = [IsAuthenticated]
    serializer_class = VacanciesNameSerializer

    def get_queryset(self):
        queryset = Vacancies.objects.values('descr').order_by('descr').distinct()

        return queryset


class VacanciesSearchView(APIView):
    ''' Возвращает все book'''
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        vacansy_search = {}
        vacansy_search['vacancies_names'] = Vacancies.objects.all()
        vacansy_search['city'] = City.objects.all()
        vacansy_search['office'] = Office.objects.all()
        vacansy_search['department'] = Department.objects.all()
        vacansy_search['status'] = VacancyStatus.objects.all()
        vacansy_search['celebrated'] = ProxyUser.objects.all()
        vacansy_search['priority'] = Priority.objects.all()
        vacansy_search['reasonForOpening'] = ReasonForOpening.objects.all()

        serializer = VacanciesSerachSerializer(vacansy_search)

        return Response(serializer.data)


class VacanciesBookView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    '''Возвращает список всех вакансий
       и количество кандидатов в каждой вакансии
       может принимать параметр 'q' для фильтрации
       Сортировка по статусу 'asc'
    '''
    permission_classes = [IsAuthenticated]
    pagination_class = None
    queryset = Vacancies.objects.filter(status=1)
    serializer_class = VacanciesBookSerializer
