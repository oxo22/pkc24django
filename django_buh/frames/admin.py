from django.contrib import admin

# Register your models here.
from frames.models.interview import Interview
from frames.models.person import Person, PassportData, FamilyComposition, EducationalInstitution, \
 FilesPerson
from frames.models.vacancies import Vacancies

admin.site.register(Person)
admin.site.register(PassportData)
admin.site.register(FamilyComposition)
admin.site.register(EducationalInstitution)
admin.site.register(FilesPerson)
admin.site.register(Vacancies)
admin.site.register(Interview)
