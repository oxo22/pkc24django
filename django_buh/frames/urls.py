from django.urls import path
from rest_framework.routers import DefaultRouter

from frames.views.interviews import InterviewModelView, InterviewCalendarList
from frames.views.vacansies import VacanciesModelView, VacanciesListView, VacanciesBookView, VacanciesSearchView
from frames.views.person import PhotoUploadView, ResumeUploadView, GetPersonFileModelView, \
    PersonsModelView, PersonsListView, PersonPhoneModelView, PersonStateModelView, PkcPersonsModelView, \
    PkcPersonPhoneModelView, PersonsStaffListView, ProposalPersonView

router = DefaultRouter()

router.register(r'persons', PersonsModelView, basename='person')
router.register(r'proposal', ProposalPersonView, basename='proposal')
router.register(r'pkc_persons', PkcPersonsModelView, basename='pkc_persons')
router.register(r'pkc_person_phone', PkcPersonPhoneModelView, basename='pkc_person_phone')
router.register(r'person_phone', PersonPhoneModelView, basename='person_phone')
router.register(r'person_state', PersonStateModelView, basename='person_state')
router.register(r'vacancies', VacanciesModelView, basename='vacancies')
router.register(r'get_person_file', GetPersonFileModelView, basename='vacancies')
router.register(r'get_interview_date', InterviewModelView, basename='interview')
router.register(r'vacancies_book', VacanciesBookView, basename='vacancies_book')
router.register(r'get_interview_date_list', InterviewCalendarList, basename='interview_filtred_by_date')
#
urlpatterns = [
    path('vacancies/list', VacanciesListView.as_view()),
    path('vacancies/vacancies_search_list', VacanciesSearchView.as_view()),
    path('persons/list', PersonsListView.as_view()),
    path('persons/staff_list/', PersonsStaffListView.as_view()),
    path(r'photo-upload/', PhotoUploadView.as_view()),
    path(r'resume-upload/', ResumeUploadView.as_view()),
]

urlpatterns += router.urls
