from book.serializers.books import ExperienceCandidateSerializer, PositionSerializer, WhereFindOutSerializer, \
    CitySerializer, CandidateStatusSerializer, ReasonForDismissalSerializer, OfficeSerializer
from book.serializers.person import GenderSerializer, PersonStatusSerializer
from rest_framework import serializers

from first.serializer import UserSerializer
from frames.models.person import Person, FilesPerson, PersonPhone, CandidateState, ProposalPerson
from frames.serializers.interview import InterviewSerializer, CandidateStateSerializer
from frames.serializers.vacancie import VacanciesSerializer


class FilesSerializer(serializers.ModelSerializer):
    person_id = serializers.IntegerField(write_only=True)
    file = serializers.CharField()

    class Meta:
        model = FilesPerson
        fields = ('__all__')


class IsFilesPersonSerializer(serializers.ListSerializer):
    '''
        Фильтр юзеров по полю person, возвращает тех у кого поле person не null
    '''

    def to_representation(self, data):
        data = data.filter(descr='PH')
        return super().to_representation(data)


# Серилизатор фото персоны
class PhotoFilesSerializer(serializers.ModelSerializer):
    person_id = serializers.IntegerField(write_only=True)
    create_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    change_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    file = serializers.CharField()

    class Meta:
        model = FilesPerson
        fields = ('__all__')
        list_serializer_class = IsFilesPersonSerializer


class PersonPhoneSerializer(serializers.ModelSerializer):
    person_id = serializers.IntegerField(write_only=True)
    create_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    change_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)

    class Meta:
        model = PersonPhone
        fields = ('__all__')


class PersonCandidateStateSerializer(serializers.ModelSerializer):
    candidate_status = CandidateStatusSerializer(read_only=True)

    person_id = serializers.IntegerField(write_only=True, allow_null=True)
    candidate_status_id = serializers.IntegerField(write_only=True)
    create_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    change_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)

    class Meta:
        model = CandidateState
        fields = ('__all__')



class PersonsStaffSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='get_full_name')

    class Meta:
        model = Person
        fields = [
            'id',
            'full_name',
        ]



class ProposalPersonSerializer(serializers.ModelSerializer):
    candidate = PersonsStaffSerializer(read_only=True)
    proposal_person = PersonsStaffSerializer(read_only=True)
    office = OfficeSerializer(read_only=True)


    candidate_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    proposal_person_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    office_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)


    class Meta:
        model = ProposalPerson
        fields = ['id',
                  'candidate',
                  'proposal_person',
                  'office',
                  'candidate_id',
                  'proposal_person_id',
                  'office_id']



# Серилизатор персоны
class PersonSerializer(serializers.ModelSerializer):
    person_status = PersonStatusSerializer(read_only=True)
    gender = GenderSerializer(read_only=True)
    candidate_experience = ExperienceCandidateSerializer(read_only=True)
    position = PositionSerializer(read_only=True)
    where_find_out = WhereFindOutSerializer(read_only=True)
    vacancies = VacanciesSerializer(read_only=True)
    photo_person = PhotoFilesSerializer(read_only=True, many=True)
    fact_city = CitySerializer(read_only=True)
    interview_frames_person = InterviewSerializer(read_only=True, many=True)
    create_date_time = serializers.DateTimeField(format("%d-%m-%Y"), read_only=True)
    person_phone_frames_person = PersonPhoneSerializer(read_only=True, many=True)
    candidate_state_frames_person = CandidateStateSerializer(read_only=True, many=True)
    reason_for_dismissal = ReasonForDismissalSerializer(read_only=True)
    celebrated = UserSerializer(read_only=True)
    proposal_person_frames_candidate = ProposalPersonSerializer(read_only=True)

    gender_id = serializers.IntegerField(write_only=True, allow_null=True)
    candidate_experience_id = serializers.IntegerField(write_only=True, allow_null=True)
    position_id = serializers.IntegerField(write_only=True, allow_null=True)
    where_find_out_id = serializers.IntegerField(write_only=True, allow_null=True)
    person_status_id = serializers.IntegerField(write_only=True, allow_null=True)
    vacancies_id = serializers.IntegerField(write_only=True, allow_null=True)
    fact_city_id = serializers.IntegerField(write_only=True, allow_null=True)
    create_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    change_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    reason_for_dismissal_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    celebrated_id = serializers.IntegerField(write_only=True,  allow_null=True, required=False)

    class Meta:
        model = Person
        fields = ('__all__')



class PkcPersonPhoneSerializer(serializers.ModelSerializer):
    person_id = serializers.IntegerField(write_only=True)
    create_user_id = serializers.IntegerField(write_only=True, allow_null=True)

    class Meta:
        model = PersonPhone
        fields = [
            'id',
            'person_id',
            'create_user_id',
            'phone',
            'telegram',
            'whats_app',
            'viber',
        ]


class PkcPersonSerializer(serializers.ModelSerializer):
    person_status = PersonStatusSerializer(read_only=True)
    gender = GenderSerializer(read_only=True)
    candidate_experience = ExperienceCandidateSerializer(read_only=True)
    position = PositionSerializer(read_only=True)
    where_find_out = WhereFindOutSerializer(read_only=True)
    fact_city = CitySerializer(read_only=True)
    person_phone_person = PkcPersonPhoneSerializer(read_only=True, many=True)

    gender_id = serializers.IntegerField(write_only=True, allow_null=True)
    candidate_experience_id = serializers.IntegerField(write_only=True, allow_null=True)
    position_id = serializers.IntegerField(write_only=True, allow_null=True)
    where_find_out_id = serializers.IntegerField(write_only=True, allow_null=True)
    person_status_id = serializers.IntegerField(write_only=True, allow_null=True)
    fact_city_id = serializers.IntegerField(write_only=True, allow_null=True)
    create_user_id = serializers.IntegerField(write_only=True, allow_null=True)

    class Meta:
        model = Person
        fields = [
            'id',
            'gender_id',
            'candidate_experience_id',
            'position_id',
            'where_find_out_id',
            'person_status_id',
            'fact_city_id',
            'create_user_id',
            'last_name',
            'first_name',
            'middle_name',
            'birthday',
            'wage_amount',
            'note',
            'person_status',
            'gender',
            'candidate_experience',
            'position',
            'where_find_out',
            'fact_city',
            'person_phone_person',
        ]

