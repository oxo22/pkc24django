from rest_framework import serializers

from book.serializers.books import CandidateStatusSerializer
from first.serializer import UserSerializer
from frames.models.interview import Interview
from frames.models.person import Person, CandidateState, PersonPhone


class IsCandidateStateSerializer(serializers.ListSerializer):
    '''
        Фильтр юзеров по полю person, возвращает тех у кого поле person не null
    '''

    def to_representation(self, data):
        # print(data)
        data = data.filter()[:1]
        return super().to_representation(data)


class CandidateStateSerializer(serializers.ModelSerializer):
    candidate_status = CandidateStatusSerializer(read_only=True)

    class Meta:
        model = CandidateState
        fields = ('__all__')
        list_serializer_class = IsCandidateStateSerializer




class InterviewPersonPhoneSerializer(serializers.ModelSerializer):


    class Meta:
        model = PersonPhone
        fields = ('__all__')




class InterviewPersonSerializer(serializers.ModelSerializer):

    candidate_state_frames_person = CandidateStateSerializer(read_only=True ,many=True)
    person_phone_frames_person = InterviewPersonPhoneSerializer(read_only=True, many=True)

    class Meta:
        model = Person
        fields = ('__all__')


# Серилизвтор собеседования
class InterviewSerializer(serializers.ModelSerializer):

    person = InterviewPersonSerializer(read_only=True)
    celebrated = UserSerializer(read_only=True)
    interview_date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M")


    person_id = serializers.IntegerField(write_only=True)
    celebrated_id = serializers.IntegerField(write_only=True)
    create_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    change_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)

    class Meta:
        model = Interview
        fields = ('__all__')





# Серилизвтор собеседования
class InterviewCalendarListSerializer(serializers.ModelSerializer):

    person = InterviewPersonSerializer(read_only=True)


    class Meta:
        model = Interview
        fields = ('__all__')

