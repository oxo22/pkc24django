from rest_framework import serializers

from book.models import Office,Department
from book.serializers.books import VacancyStatusSerializer, TypeOfEmploymentSerializer, ExperienceCandidateSerializer, \
    DepartmentSerializer, PrioritySerializer, CitySerializer, OfficeSerializer, CandidateStatusSerializer, \
    ReasonForOpeningSerializer
from first.serializer import UserSerializer
from frames.models.person import Person, FilesPerson, CandidateState
from frames.models.vacancies import Vacancies


class IsFilesVacanciePersonSerializer(serializers.ListSerializer):
    '''
        Фильтр персон у которых есть фото
    '''

    def to_representation(self, data):
        data = data.filter(descr = 'PH')
        return super().to_representation(data)



class IsCandidateStatePersonSerializer(serializers.ListSerializer):
    '''
        Фильтр персон у которых есть фото
    '''

    def to_representation(self, data):
        data = data.all()[:1]
        return super().to_representation(data)


class VacanciesPhotoFilesSerializer(serializers.ModelSerializer):

    person_id = serializers.IntegerField(write_only=True)
    file = serializers.CharField()

    class Meta:
        model = FilesPerson
        fields = ('__all__')
        list_serializer_class = IsFilesVacanciePersonSerializer


class CandidateVacanciesStateSerializer(serializers.ModelSerializer):

    candidate_state_book_candidate_status = CandidateStatusSerializer(read_only=True)

    class Meta:
        model = CandidateState
        fields = ('__all__')
        list_serializer_class = IsCandidateStatePersonSerializer


class VacanciesPersonSerializer(serializers.ModelSerializer):

    photo_person_frames_person = VacanciesPhotoFilesSerializer(read_only=True, many=True)
    candidate_state_frames_person = CandidateVacanciesStateSerializer(read_only=True, many=True)

    class Meta:
        model = Person
        fields = ('__all__')





class VacanciesSerializer(serializers.ModelSerializer):

    status = VacancyStatusSerializer(read_only=True)
    type_of_employment = TypeOfEmploymentSerializer(read_only=True)
    experience_candidate = ExperienceCandidateSerializer(read_only=True)
    department = DepartmentSerializer(read_only=True)
    office = OfficeSerializer(read_only=True)
    priority = PrioritySerializer(read_only=True)
    city = CitySerializer(read_only=True)
    celebrated = UserSerializer(read_only=True)
    person_doc_vacancies = VacanciesPersonSerializer(read_only=True, many=True)
    reason_for_opening = ReasonForOpeningSerializer(read_only=True)

    status_id = serializers.IntegerField(write_only=True)
    type_of_employment_id = serializers.IntegerField(write_only=True)
    experience_candidate_id = serializers.IntegerField(write_only=True)
    department_id = serializers.IntegerField(write_only=True)
    office_id = serializers.IntegerField(write_only=True)
    priority_id = serializers.IntegerField(write_only=True)
    city_id = serializers.IntegerField(write_only=True)
    celebrated_id = serializers.IntegerField(write_only=True)
    reason_for_opening_id = serializers.IntegerField(write_only=True)
    create_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)
    change_user_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)

    class Meta:
        model = Vacancies
        fields = ('__all__')


class VacanciesListSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    reason_for_opening__descr = serializers.CharField()
    descr = serializers.CharField()
    celebrated__person__last_name = serializers.CharField()
    city__descr = serializers.CharField()
    office__item = serializers.CharField()
    department__descr = serializers.CharField()
    status__descr = serializers.CharField()
    count_candidate = serializers.IntegerField()
    priority__descr = serializers.CharField()



class VacanciesDepartmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Department
        fields = ['id','descr']


class VacanciesOfficeSerializer(serializers.ModelSerializer):



    class Meta:
        model = Office
        fields = ['id','item']


class VacanciesBookSerializer(serializers.ModelSerializer):

    office = VacanciesOfficeSerializer(read_only=True)
    department = VacanciesDepartmentSerializer(read_only=True)

    class Meta:
        model = Vacancies
        fields = ['id', 'descr', 'office','department']




class VacanciesNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vacancies
        fields = ['descr']





class VacanciesSerachSerializer(serializers.Serializer):

    vacancies_names = VacanciesNameSerializer(read_only=True, many=True)
    city = CitySerializer(read_only=True, many=True)
    office = OfficeSerializer(read_only=True, many=True)
    department = DepartmentSerializer(read_only=True, many=True)
    celebrated = UserSerializer(read_only=True, many=True)
    status = VacancyStatusSerializer(read_only=True, many=True)
    priority = PrioritySerializer(read_only=True, many=True)
    reasonForOpening = ReasonForOpeningSerializer(read_only=True, many=True)

