from rest_framework.routers import DefaultRouter

from administration.views.group import DjangoGroupModelView, VueGroupModelView, DjangoAddDeleteGroupModelView, \
    VueAddDeleteGroupModelView
from administration.views.menu import RoleModelView, MainMenuModelView, NodePermissionsModelView, AllMenuModelView
from administration.views.user import UserModelView, UserCheckModelView, UserPasswordModelView

router = DefaultRouter()

router.register(r'role', RoleModelView, basename='role')
router.register(r'main_menu', MainMenuModelView, basename='main_menu')
router.register(r'user', UserModelView, basename='user')
router.register(r'user_password', UserPasswordModelView, basename='user_password')
router.register(r'django_all_groups', DjangoGroupModelView, basename='django_all_groups')
router.register(r'django_add_del_group', DjangoAddDeleteGroupModelView, basename='django_add_del_group')
router.register(r'vue_add_del_group', VueAddDeleteGroupModelView, basename='vue_add_del_group')
router.register(r'vue_all_groups', VueGroupModelView, basename='vue_all_groups')
router.register(r'username_check', UserCheckModelView, basename='username_check')
router.register(r'all_menu', AllMenuModelView, basename='all_menu')
router.register(r'node_permissions', NodePermissionsModelView, basename='node_permissions')

urlpatterns = [

]

urlpatterns += router.urls
