from django_buh import settings
from first.basemodels import Common
from django.contrib.contenttypes.models import ContentType
from django.db import models


class Bar(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             verbose_name='Справочник меню',
                             blank=False,
                             help_text='Справочник меню',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'teh_bar'
        verbose_name = 'Справочник меню'
        verbose_name_plural = 'Справочник меню'


class BarSite(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             verbose_name='Справочник меню какого сайта',
                             blank=False,
                             help_text='Справочник меню какого сайта',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'teh_bar_site'
        verbose_name = 'Справочник меню какого сайта'
        verbose_name_plural = 'Справочник меню какого сайта'


class BarApp(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             verbose_name='Справочник меню приложений',
                             blank=False,
                             help_text='Справочник меню приложений',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'teh_bar_app'
        verbose_name = 'Справочник меню приложений'
        verbose_name_plural = 'Справочник меню приложений'


class MainMenu(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             verbose_name='Главное меню',
                             blank=True,
                             null=True,
                             help_text='Главное меню',
                             unique=True)
    url = models.CharField(max_length=255,
                           verbose_name='Главное меню url',
                           blank=True,
                           null=True,
                           help_text='Главное меню url', )
    icons = models.CharField(max_length=255,
                             verbose_name='Главное меню icons',
                             blank=True,
                             null=True,
                             help_text='Главное меню icons')
    bar = models.ForeignKey(Bar,
                            on_delete=models.CASCADE,
                            related_name='main_menu_administration_bar',
                            blank=False,
                            verbose_name='Справочник меню',
                            help_text='Ссылка на таблицу teh_bar')
    bar_site = models.ForeignKey(BarSite,
                                 on_delete=models.CASCADE,
                                 related_name='main_menu_administration_bar_site',
                                 blank=True,
                                 null=True,
                                 verbose_name='Справочник меню какого сайта',
                                 help_text='Ссылка на таблицу teh_bar_site')
    bar_app = models.ForeignKey(BarApp,
                                on_delete=models.CASCADE,
                                related_name='main_menu_administration_bar_app',
                                blank=True,
                                null=True,
                                verbose_name='Справочник меню приложений',
                                help_text='Ссылка на таблицу teh_bar_app')

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'teh_main_menu'
        verbose_name = 'Главное меню'
        verbose_name_plural = 'Главное меню'


class MenuComponents(Common):
    id = models.AutoField(primary_key=True)
    main_menu = models.ForeignKey(MainMenu,
                                  on_delete=models.CASCADE,
                                  related_name='menu_components_administration_main_menu',
                                  blank=False,
                                  verbose_name='Главное меню',
                                  help_text='Ссылка на таблицу teh_main_menu')
    descr = models.CharField(max_length=255,
                             verbose_name='Компоненты меню',
                             blank=True,
                             null=True,
                             help_text='Компоненты меню')
    url = models.CharField(max_length=255,
                           verbose_name='Компоненты меню url',
                           blank=True,
                           null=True,
                           help_text='Компоненты меню url')
    icons = models.CharField(max_length=255,
                             verbose_name='Компоненты меню icons',
                             blank=True,
                             null=True,
                             help_text='Компоненты меню icons')

    def __str__(self):
        return 'id: %s - %s - %s' % (self.id, self.descr, self.main_menu)

    class Meta:
        db_table = 'teh_menu_components'
        verbose_name = 'Компоненты меню'
        verbose_name_plural = 'Компоненты меню'


class Permissions(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             verbose_name='Права доступа',
                             blank=False,
                             help_text='Права доступа',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'teh_permissions'
        verbose_name = 'Права доступа'
        verbose_name_plural = 'Права доступа'


class Role(Common):
    id = models.AutoField(primary_key=True)

    descr = models.CharField(max_length=255,
                             verbose_name='Роли доступа',
                             blank=False,
                             help_text='Роли доступа',
                             unique=True)
    note = models.TextField(blank=True,
                            null=True,
                            verbose_name='Коммент к роли',
                            help_text='Коммент к роли')
    user = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                  related_name='role_administration_user',
                                  blank=False,
                                  verbose_name='Пользователь системы',
                                  help_text='Ссылка на таблицу first_user')

    def __str__(self):
        return 'id: %s - %s ' % (self.id, self.descr)

    class Meta:
        db_table = 'teh_role'
        verbose_name = 'Роли доступа'
        verbose_name_plural = 'Роли доступа'


class NodePermissions(Common):
    id = models.AutoField(primary_key=True)
    menu_components = models.ForeignKey(MenuComponents,
                                        on_delete=models.CASCADE,
                                        related_name='node_permissions_administration_menu_components',
                                        blank=False,
                                        verbose_name='Компонент меню',
                                        help_text='Ссылка на таблицу teh_menu_components')
    permissions = models.ForeignKey(Permissions,
                                    on_delete=models.CASCADE,
                                    related_name='node_permissions_administration_permissions',
                                    blank=False,
                                    verbose_name='Компонент меню',
                                    help_text='Ссылка на таблицу teh_permissions')
    role = models.ForeignKey(Role,
                             on_delete=models.CASCADE,
                             related_name='node_permissions_administration_group_role',
                             blank=False,
                             verbose_name='Компонент меню',
                             help_text='Ссылка на таблицу teh_group_role')

    def __str__(self):
        return 'id: %s - %s %s %s' % (self.id, self.menu_components, self.permissions, self.role)

    class Meta:
        db_table = 'teh_node_permissions'
        verbose_name = 'Права Узлов дерева элементов системы'
        verbose_name_plural = 'Права Узлов дерева элементов системы'
        unique_together = ('menu_components', 'role')
