from django.contrib.auth.models import Group
from administration.models import Role
from administration.serializers.group import ProxyUserDjangoGroupsSerializer, ProxyUserVueGroupsSerializer, \
    DjangoAddDeleteGroupModelSerializer, VueAddDeleteGroupModelSerializer
from first.classes import ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR
from first.models import ProxyUser
from first.permissions import IsMemberGroup


class DjangoGroupModelView(ListPermissionViewSetsPR):
    """
         класс списка всех групп Django
     """
    permission_classes = [IsMemberGroup]
    pagination_class = None
    queryset = Group.objects.all()
    serializer_class = ProxyUserDjangoGroupsSerializer


class VueGroupModelView(ListPermissionViewSetsPR):
    """
             класс списка всех ролей Vue
    """
    permission_classes = [IsMemberGroup]
    pagination_class = None

    serializer_class = ProxyUserVueGroupsSerializer

    def get_queryset(self):
        """
            если принимает user_id фильтрует по нему, иначе возвращает все роли
        """
        user_id = self.request.GET.get('user_id')

        if user_id:
            queryset = Role.objects.filter(user=user_id)
        else:
            queryset = Role.objects.all()
        return queryset.exclude(descr='admin')


class DjangoAddDeleteGroupModelView(ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR):
    """
        класс добавления/удаления групп Django
    """
    permission_classes = [IsMemberGroup]
    serializer_class = DjangoAddDeleteGroupModelSerializer

    def get_queryset(self):
        # Забираем переменные с запроса
        action = self.request.GET.get('action')
        user_id = self.request.GET.get('user_id')
        group_id = self.request.GET.get('group_id')

        # Получаем группу и пользователя
        group = Group.objects.get(pk=group_id)
        user = ProxyUser.objects.get(pk=user_id)

        # Если action==add - добавляем пользователя в группу,
        # Если action==remove - удаляем пользователя из группы
        if action == 'add':
            user.groups.add(group)
        elif action == 'remove':
            user.groups.remove(group)

        res = Group.objects.filter(user=user)
        return res


class VueAddDeleteGroupModelView(ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR):
    """
        класс добавления/удаления групп Vue
    """

    permission_classes = [IsMemberGroup]
    serializer_class = VueAddDeleteGroupModelSerializer

    def get_queryset(self):
        # Забираем переменные с запроса
        action = self.request.GET.get('action')
        user_id = self.request.GET.get('user_id')
        role_id = self.request.GET.get('role_id')

        # Получаем роль и пользователя
        # some_method(user_id, group_id)
        user = ProxyUser.objects.get(pk=user_id)
        role = Role.objects.get(pk=role_id)

        # Если action==add - добавляем пользователя в роль,
        # Если action==remove - удаляем пользователя из роли
        if action == 'add':
            role.user.add(user)
        elif action == 'remove':
            role.user.remove(user)

        res = Role.objects.filter(user=user)
        return res
