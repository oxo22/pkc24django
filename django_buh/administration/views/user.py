from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from administration.serializers.user import ProxyUserCheckSerializer, ProxyUserSerializer, ProxyUserPasswordSerializer
from first.classes import ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR
from first.models import ProxyUser


class UserCheckModelView(ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR):
    """
           Класс юзера
    """
    permission_classes = [IsAuthenticated]
    serializer_class = ProxyUserCheckSerializer

    def get_queryset(self):
        username = self.request.GET.get('username')
        query = ProxyUser.objects.filter(username=username)

        return query


class UserModelView(ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR):
    """
        Класс сотрудников
    """
    permission_classes = [IsAuthenticated]
    serializer_class = ProxyUserSerializer

    def get_queryset(self):
        flag = self.request.GET.get('flag')
        search = self.request.GET.get('search')

        if flag:
            queryset = ProxyUser.objects.all()

        if search:
            for word in search.split():
                queryset = ProxyUser.objects.filter(is_staff=False).filter(
                    Q(username__icontains=word) |
                    Q(person__last_name__icontains=word) |
                    Q(person__first_name__icontains=word) |
                    Q(person__middle_name__icontains=word) |
                    Q(first_name__icontains=word) |
                    Q(person__position__descr__icontains=word) |
                    Q(role_administration_user__descr__icontains=word) |
                    Q(last_name__icontains=word)
                )

        else:
            queryset = ProxyUser.objects.filter(is_staff=False)

        return queryset.distinct()


class UserPasswordModelView(ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR):
    """
        Класс пароля
    """
    permission_classes = [IsAuthenticated]
    serializer_class = ProxyUserPasswordSerializer

    def get_queryset(self):
        if self.request.GET.get('flag'):
            queryset = ProxyUser.objects.all()
        else:
            queryset = ProxyUser.objects.filter(is_staff=False)

        return queryset
