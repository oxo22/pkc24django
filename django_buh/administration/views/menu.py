from django.db.models import Q
from rest_framework.permissions import IsAuthenticated

from administration.models import Role, NodePermissions, MenuComponents
from administration.serializers.menu import RoleSerializer, NodePermissionsSerializer, MenuComponentsSerializer
from first.classes import CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR



class RoleModelView(ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR):
    """
        Класс справочник баров
    """
    permission_classes = [IsAuthenticated]
    queryset = Role.objects.all()
    serializer_class = RoleSerializer


class NodePermissionsModelView(ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR):
    """
        Класс справочник баров
    """
    permission_classes = [IsAuthenticated]
    pagination_class = None
    serializer_class = NodePermissionsSerializer

    filter_fields = ("question", "answer")

    update_data_pk_field = 'id'

    # Функция уоторая выполняет create on update
    def create(self, request, *args, **kwargs):
        try:
            if request.data['id']:
                kwarg_field: str = self.lookup_url_kwarg or self.lookup_field
                self.kwargs[kwarg_field] = request.data[self.update_data_pk_field]
                return self.update(request, *args, **kwargs)
        except:
            try:
                return super().create(request, *args, **kwargs)
            except:
                print('Дубликат полей')



    def get_queryset(self, **kwargs):
        role_id = self.request.GET.get('role_id')
        if role_id:
            query = NodePermissions.objects.filter(role_id=role_id)
        else:
            query = NodePermissions.objects.all()

        return query



class MainMenuModelView(ListPermissionViewSetsPR):
    """
        Класс отображения Меню и права доступа пользователя
    """
    permission_classes = [IsAuthenticated]
    pagination_class = None
    serializer_class = NodePermissionsSerializer

    def get_queryset(self, **kwargs):
        """
        Для доступа с ролью admin (role_id = 1) возвращает все пункты меню
        """

        website = self.request.GET.get('website')
        role = Role.objects.values('id').filter(user=self.request.user.id).filter(id=1)
        if len(role) > 0:
            menu = NodePermissions.objects.all()
        else:
            menu = NodePermissions.objects. \
                filter(
                role__user=self.request.user.id)
        if website:
            menu = menu.filter(
                Q(menu_components__main_menu__bar_site__descr=website) |
                Q(menu_components__main_menu__bar_site__isnull=True)
                                                  )
        return menu


class AllMenuModelView(ListPermissionViewSetsPR):
    """
        Класс отображения Меню и права доступа пользователя
    """
    permission_classes = [IsAuthenticated]
    pagination_class = None
    queryset = MenuComponents.objects.all()
    serializer_class = MenuComponentsSerializer
