from rest_framework import serializers

from administration.models import Bar, MainMenu, Role, NodePermissions, Permissions, BarApp, BarSite, MenuComponents


class BarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bar
        fields = ['id', 'descr']


class PermissionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permissions
        fields = ['id', 'descr']


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = ['id', 'descr', 'note']


class BarAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = BarApp
        fields = ['id', 'descr']


class BarSiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = BarSite
        fields = ['id', 'descr']


class MainMenuSerializer(serializers.ModelSerializer):
    bar = BarSerializer(read_only=True)
    bar_app = BarAppSerializer(read_only=True)
    bar_site = BarSiteSerializer(read_only=True)

    class Meta:
        model = MainMenu
        fields = ['id', 'descr', 'url', 'icons', 'bar', 'bar_app', 'bar_site']


class MenuComponentsSerializer(serializers.ModelSerializer):
    main_menu = MainMenuSerializer(read_only=True)

    class Meta:
        model = MenuComponents
        fields = ['main_menu', 'id', 'descr', 'url', 'icons']


class NodePermissionsSerializer(serializers.ModelSerializer):
    permissions = PermissionsSerializer(read_only=True)
    role = RoleSerializer(read_only=True)
    menu_components = MenuComponentsSerializer(read_only=True)

    permissions_id = serializers.IntegerField(write_only=True)
    role_id = serializers.IntegerField(write_only=True)
    menu_components_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = NodePermissions
        fields = ['id', 'permissions', 'role', 'menu_components', 'permissions_id', 'role_id', 'menu_components_id']
