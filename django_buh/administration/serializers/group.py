from django.contrib.auth.models import Group
from rest_framework import serializers

from administration.models import Role


class ProxyUserDjangoGroupsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ['name', 'id']


class DjangoAddDeleteGroupModelSerializer(serializers.ModelSerializer):


    class Meta:
        model = Group
        fields = ['id', 'name']


class ProxyUserVueGroupsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        fields = [ 'id', 'descr']


class VueAddDeleteGroupModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        fields = ['id', 'descr']