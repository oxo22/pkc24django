from rest_framework import serializers

from administration.serializers.group import ProxyUserDjangoGroupsSerializer, ProxyUserVueGroupsSerializer
from book.serializers.books import PositionSerializer
from first.models import ProxyUser
from frames.models.person import Person


class ProxyUserPersonSerializer(serializers.ModelSerializer):
    position = PositionSerializer(read_only=True)
    class Meta:
        model = Person
        fields = [
            'id',
            'last_name',
            'first_name',
            'middle_name',
            'person_status',
            'position',
        ]


class ProxyUserCheckSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProxyUser
        fields = ['id', 'username']


class ProxyUserPasswordSerializer(serializers.ModelSerializer):
    username = serializers.CharField(write_only=True, required=False)
    password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = ProxyUser
        fields = ['id', 'username', 'password']

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return instance


class ProxyUserSerializer(serializers.ModelSerializer):
    person = ProxyUserPersonSerializer(read_only=True)
    groups = ProxyUserDjangoGroupsSerializer(read_only=True, many=True)
    role_administration_user = ProxyUserVueGroupsSerializer(read_only=True, many=True)

    person_id = serializers.IntegerField(write_only=True, allow_null=True, required=False)

    class Meta:
        model = ProxyUser
        fields = [
            'id',
            'username',
            'person',
            'groups',
            'person_id',
            'is_active',
            'first_name',
            'last_name',
            'role_administration_user'
        ]
