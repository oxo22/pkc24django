from django.contrib import admin

# Register your models here.
from administration.models import Bar, MainMenu, MenuComponents, Permissions, Role, NodePermissions, BarApp

admin.site.register(Bar)
admin.site.register(BarApp)
admin.site.register(MainMenu)

admin.site.register(Permissions)
admin.site.register(Role)
admin.site.register(NodePermissions)




class MenuComponentsFilter(admin.ModelAdmin):
    list_display = ('descr','main_menu',)
    list_filter = ('main_menu',)

admin.site.register(MenuComponents,MenuComponentsFilter)