from django.conf import settings
from django.db import models
from first.basemodels import Common


class Country(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             verbose_name='Страна',
                             blank=False,
                             help_text='Страна',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_country'
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'


class Gender(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=254,
                             verbose_name='Пол',
                             blank=False,
                             help_text='Пол',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_gender'
        verbose_name = 'Пол'
        verbose_name_plural = 'Пол'


class City(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             verbose_name='Город',
                             blank=False,
                             help_text='Город',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_city'
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class Citizenship(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Гражданство',
                             help_text='Гражданство',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_сitizenship'
        verbose_name = 'Гражданство'
        verbose_name_plural = 'Гражданство'


class MaritalStatus(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Состояние в браке',
                             help_text='Состояние в браке',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_marital_status'
        verbose_name = 'Семейное положение'
        verbose_name_plural = 'Семейное положение'


class RelationDegree(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Степень родства',
                             help_text='Степень родства',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_relation_degree'
        verbose_name = 'Степень родства'
        verbose_name_plural = 'Степень родства'


class Education(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Вид образования',
                             help_text='Вид образования',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_education'
        verbose_name = 'Вид образования'
        verbose_name_plural = 'Виды образования'


class PersonStatus(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Статус персоны',
                             help_text='Статус персоны',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_person_status'
        verbose_name = 'Статус персоны'
        verbose_name_plural = 'Статусы персоны'


class CandidateStatus(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Статус кандидата',
                             help_text='Статус кандидата',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_candidate_status'
        verbose_name = 'Статус кандидата'
        verbose_name_plural = 'Статусы кандидата'


class Position(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Должность',
                             help_text='Должность',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_position'
        verbose_name = 'Должность'
        verbose_name_plural = 'Должность'


class Bush(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Название куста',
                             help_text='Название куста',
                             unique=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.PROTECT,
                             related_name='bush_first_auth_user',
                             null=True,
                             blank='True',
                             verbose_name='Пользователь системы.Руководитель куста',
                             help_text='Ссылка на таблицу auth_user')

    def __str__(self):
        return 'id: %s  %s  %s ' % (self.id, self.descr, self.user)

    class Meta:
        db_table = 'book_bush'
        verbose_name = 'Куст'
        verbose_name_plural = 'Кусты'


class Office(Common):
    id = models.AutoField(primary_key=True)
    item = models.CharField(max_length=255,
                            blank=True,
                            verbose_name='Отделения',
                            help_text='Отделения',
                            unique=True)
    descr = models.CharField(max_length=255,
                             blank=True,
                             verbose_name='Описание отделения',
                             help_text='Описание отделения')
    city = models.ForeignKey(City,
                             on_delete=models.PROTECT,
                             related_name='office_book_city',
                             null=True,
                             blank=True,
                             verbose_name='Город',
                             help_text='Ссылка на таблицу book_city')
    address = models.CharField(max_length=255,
                               blank=True,
                               verbose_name='Отделения',
                               help_text='Отделения')
    bush = models.ForeignKey(Bush,
                             on_delete=models.PROTECT,
                             related_name='office_book_bush',
                             null=True,
                             blank='True',
                             verbose_name='Куст',
                             help_text='Ссылка на таблицу book_bush')
    opening_date = models.DateField(blank=True,
                                    null=True,
                                    verbose_name='Дата открытия отделения',
                                    help_text='Дата открытия отделения')
    closing_date = models.DateField(blank=True,
                                    null=True,
                                    verbose_name='Дата закрытия отделения',
                                    help_text='Дата закрытия отделения')
    pic_photo = models.URLField(max_length=255,
                                blank=True,
                                verbose_name='Отделения',
                                help_text='Отделения')

    def __str__(self):
        return 'id: %s  %s  %s ' % (
            self.id, self.item, self.address)

    class Meta:
        db_table = 'book_office'
        verbose_name = 'Отделение'
        verbose_name_plural = 'Отделения'


class Department(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Отделы',
                             help_text='Отделы',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_department'
        verbose_name = 'Отдел'
        verbose_name_plural = 'Отделы'


class ReasonForOpening(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Причина окрытия',
                             help_text='Причина окрытия',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_reason_for_opening'
        verbose_name = 'Причина окрытия'
        verbose_name_plural = 'Причина окрытия'


class VacancyStatus(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Статус вакансии',
                             help_text='Статус вакансии',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_vacancy_status'
        verbose_name = 'Статус вакансии'
        verbose_name_plural = 'Статус вакансий'


class WhereFindOut(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Откуда узнали о_ вакансии',
                             help_text='Откуда узнали о_ вакансии', unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_where_find_out'
        verbose_name = 'Откуда узнали о_ вакансии'
        verbose_name_plural = 'Откуда узнали о_ вакансии'


class ExperienceCandidate(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Опыт кандидата',
                             help_text='Опыт кандидата',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_experience_candidate'
        verbose_name = 'Опыт кандидата'
        verbose_name_plural = 'Опыт кандидата'


class TypeOfEmployment(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Вид занятости',
                             help_text='Вид занятости',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_type_of_employment'
        verbose_name = 'Вид занятости'
        verbose_name_plural = 'Вид занятости'


class ReasonForDismissal(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Причина увольнения',
                             help_text='Причина увольнения',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_reason_for_dismissal'
        verbose_name = 'Причина увольнения'
        verbose_name_plural = 'Причины увольнения'


class Priority(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             blank=False,
                             verbose_name='Вакансия Приоритет',
                             help_text='Вакансия Приоритет',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_priority'
        verbose_name = 'Вакансия Приоритет'
        verbose_name_plural = 'Вакансия Приоритет'


class SectionReference(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             verbose_name='Справочник разделов тестов',
                             blank=False,
                             help_text='Справочник разделов тестов',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_testing'
        verbose_name = 'Справочник разделов тестов'
        verbose_name_plural = 'Справочник разделов тестов'


class GradingCategory(Common):
    id = models.AutoField(primary_key=True)
    descr = models.CharField(max_length=255,
                             verbose_name='Справочник категорий',
                             blank=False,
                             help_text='Справочник категорий',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_grading_category'
        verbose_name = 'Справочник категорий'
        verbose_name_plural = 'Справочник категорий'
