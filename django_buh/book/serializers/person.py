from rest_framework import serializers

from book.serializers.books import CountrySerializer, GenderSerializer, CitySerializer, CitizenshipSerializer, \
    MaritalStatusSerializer, RelationDegreeSerializer, EducationSerializer, PersonStatusSerializer


class PersonBooksSerializer(serializers.Serializer):
    country = CountrySerializer(read_only=True, many=True)
    gender = GenderSerializer(read_only=True, many=True)
    city = CitySerializer(read_only=True, many=True)
    citizenship = CitizenshipSerializer(read_only=True, many=True)
    maritalStatus = MaritalStatusSerializer(read_only=True, many=True)
    relationDegree = RelationDegreeSerializer(read_only=True, many=True)
    education = EducationSerializer(read_only=True, many=True)
    personStatus = PersonStatusSerializer(read_only=True, many=True)


