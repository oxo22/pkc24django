from rest_framework import serializers
from first.serializer import UserSerializer

from book.models import Country, Gender, City, Citizenship, MaritalStatus, RelationDegree, Education, PersonStatus, \
    WhereFindOut, CandidateStatus, ExperienceCandidate, Bush, Office, Department, ReasonForOpening, \
    VacancyStatus, TypeOfEmployment, Priority, Position, ReasonForDismissal, GradingCategory


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = ['id', 'descr', 'in_archive']


class GenderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Gender
        fields = ['id', 'descr', 'in_archive']


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ['id', 'descr', 'in_archive']


class CitizenshipSerializer(serializers.ModelSerializer):

    class Meta:
        model = Citizenship
        fields = ['id', 'descr', 'in_archive']


class MaritalStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = MaritalStatus
        fields = ['id', 'descr', 'in_archive']


class RelationDegreeSerializer(serializers.ModelSerializer):

    class Meta:
        model = RelationDegree
        fields = ['id', 'descr', 'in_archive']


class EducationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Education
        fields = ['id', 'descr', 'in_archive']


class PersonStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = PersonStatus
        fields = ['id', 'descr', 'in_archive']


class CandidateStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = CandidateStatus
        fields = ['id', 'descr', 'in_archive']


class PositionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Position
        fields = ['id', 'descr', 'in_archive']


class BushSerializer(serializers.ModelSerializer):

    user = UserSerializer(read_only=True)
    user_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Bush
        fields = ['id', 'descr', 'user', 'user_id']


class OfficeSerializer(serializers.ModelSerializer):
    bush = BushSerializer(read_only=True)
    city = CitySerializer(read_only=True)
    bush_id = serializers.IntegerField(write_only=True)
    city_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Office
        fields = ['id', 'item', 'descr', 'bush', 'city', 'bush_id', 'city_id']


class OfficeListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Office
        fields = ['id', 'item']


class DepartmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Department
        fields = ['id', 'descr', 'in_archive']


class ReasonForOpeningSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReasonForOpening
        fields = ['id', 'descr', 'in_archive']


class VacancyStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = VacancyStatus
        fields = ['id', 'descr', 'in_archive']


class WhereFindOutSerializer(serializers.ModelSerializer):

    class Meta:
        model = WhereFindOut
        fields = ['id', 'descr', 'in_archive']


class ExperienceCandidateSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExperienceCandidate
        fields = ['id', 'descr', 'in_archive']


class TypeOfEmploymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = TypeOfEmployment
        fields = ['id', 'descr', 'in_archive']


class ReasonForDismissalSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReasonForDismissal
        fields = ['id', 'descr', 'in_archive']


class PrioritySerializer(serializers.ModelSerializer):

    class Meta:
        model = Priority
        fields = ['id', 'descr', 'in_archive']


class GradingCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = GradingCategory
        fields = ['id', 'descr', 'in_archive']


class BooksSerializer(serializers.Serializer):

    country = CountrySerializer(read_only=True, many=True)
    gender = GenderSerializer(read_only=True, many=True)
    city = CitySerializer(read_only=True, many=True)
    citizenship = CitizenshipSerializer(read_only=True, many=True)
    maritalStatus = MaritalStatusSerializer(read_only=True, many=True)
    relationDegree = RelationDegreeSerializer(read_only=True, many=True)
    education = EducationSerializer(read_only=True, many=True)
    personStatus = PersonStatusSerializer(read_only=True, many=True)
    candidateStatus = CandidateStatusSerializer(read_only=True, many=True)
    position = PositionSerializer(read_only=True, many=True)
    bush = BushSerializer(read_only=True, many=True)
    office = OfficeSerializer(read_only=True, many=True)
    department = DepartmentSerializer(read_only=True, many=True)
    reasonForOpening = ReasonForOpeningSerializer(read_only=True, many=True)
    vacancyStatus = VacancyStatusSerializer(read_only=True, many=True)
    whereFindOut = WhereFindOutSerializer(read_only=True, many=True)
    experienceCandidate = ExperienceCandidateSerializer(read_only=True, many=True)
    userList = UserSerializer(read_only=True, many=True)
    typeOfEmployment = TypeOfEmploymentSerializer(read_only=True, many=True)
    reasonForDismissal = ReasonForDismissalSerializer(read_only=True, many=True)
    priority = PrioritySerializer(read_only=True, many=True)
    gradingCategory = GradingCategorySerializer(read_only=True, many=True)
