from django.urls import path
from rest_framework.routers import DefaultRouter

from book.views.books import BooksView, CountryModelView, CityModelView, GenderModelView, CitizenshipModelView, \
    MaritalStatusModelView, RelationDegreeModelView, EducationModelView, PersonStatusModelView, \
    CandidateStatusModelView, PositionModelView, BushModelView, OfficeModelView, DepartmentModelView, \
    ReasonForOpeningModelView, VacancyStatusModelView, WhereFindOutModelView, ExperienceCandidateModelView, \
    TypeOfEmploymentModelView, PriorityModelView, PkcBooksView, ReasonForDismissalModelView, GradingCategoryModelView, \
    OfficeListModelView

router = DefaultRouter()

router.register(r'country', CountryModelView, basename='country')
router.register(r'city', CityModelView, basename='city')
router.register(r'gender', GenderModelView, basename='gender')
router.register(r'citizenship', CitizenshipModelView, basename='citizenship')
router.register(r'marital_status', MaritalStatusModelView, basename='marital_status')
router.register(r'relation_degree', RelationDegreeModelView, basename='relation_degree')
router.register(r'education', EducationModelView, basename='education')
router.register(r'person_status', PersonStatusModelView, basename='person_status')
router.register(r'candidate_status', CandidateStatusModelView, basename='candidate_status')
router.register(r'position', PositionModelView, basename='position')
router.register(r'bush', BushModelView, basename='bush')
router.register(r'office', OfficeModelView, basename='office')
router.register(r'office_list', OfficeListModelView, basename='office')
router.register(r'department', DepartmentModelView, basename='department')
router.register(r'reason_for_opening', ReasonForOpeningModelView, basename='reason_for_opening')
router.register(r'vacancy_status', VacancyStatusModelView, basename='vacancy_status')
router.register(r'where_find_out', WhereFindOutModelView, basename='where_find_out')
router.register(r'experience_candidate', ExperienceCandidateModelView, basename='experience_candidate')
router.register(r'type_of_employment', TypeOfEmploymentModelView, basename='type_of_employment')
router.register(r'reason_for_dismissal', ReasonForDismissalModelView, basename='reason_for_dismissal')
router.register(r'grading_category', GradingCategoryModelView, basename='grading_category')

router.register(r'priority', PriorityModelView, basename='priority')

urlpatterns = [
    path('', BooksView.as_view()),
    path('pkc/', PkcBooksView.as_view()),
]

urlpatterns += router.urls
