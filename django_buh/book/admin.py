from django.contrib import admin
from book.models import City, Country, Citizenship, Gender, Education, MaritalStatus, RelationDegree, PersonStatus, \
    Bush, SectionReference

# Register your models here.
admin.site.register(City)
admin.site.register(Gender)
admin.site.register(Country)
admin.site.register(Citizenship)
admin.site.register(Education)
admin.site.register(MaritalStatus)
admin.site.register(RelationDegree)
admin.site.register(PersonStatus)
admin.site.register(Bush)
admin.site.register(SectionReference)
