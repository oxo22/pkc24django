from first.models import ProxyUser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from book.models import City, Country, Gender, Citizenship, MaritalStatus, RelationDegree, Education, \
    PersonStatus, \
    Position, Bush, Office, Department, ReasonForOpening, VacancyStatus, WhereFindOut, ExperienceCandidate, \
    TypeOfEmployment, Priority, CandidateStatus, ReasonForDismissal, GradingCategory
from book.serializers.books import BooksSerializer, CountrySerializer, EducationSerializer, CitySerializer, \
    GenderSerializer, CitizenshipSerializer, MaritalStatusSerializer, RelationDegreeSerializer, \
    PersonStatusSerializer, \
    CandidateStatusSerializer, PositionSerializer, BushSerializer, OfficeSerializer, DepartmentSerializer, \
    ReasonForOpeningSerializer, VacancyStatusSerializer, WhereFindOutSerializer, ExperienceCandidateSerializer, \
    TypeOfEmploymentSerializer, PrioritySerializer, ReasonForDismissalSerializer, GradingCategorySerializer, \
    OfficeListSerializer
from first.classes import CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR
from first.permissions import IsMemberGroup


class BooksView(APIView):
    """
    Возвращает все book
    """
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        books = {}
        books['country'] = Country.objects.all()
        books['gender'] = Gender.objects.all()
        books['city'] = City.objects.all()
        books['citizenship'] = Citizenship.objects.all()
        books['maritalStatus'] = MaritalStatus.objects.all()
        books['relationDegree'] = RelationDegree.objects.all()
        books['education'] = Education.objects.all()
        books['personStatus'] = PersonStatus.objects.all()
        books['position'] = Position.objects.all()
        books['bush'] = Bush.objects.all()
        books['office'] = Office.objects.all()
        books['department'] = Department.objects.all()
        books['reasonForOpening'] = ReasonForOpening.objects.all()
        books['vacancyStatus'] = VacancyStatus.objects.all()
        books['whereFindOut'] = WhereFindOut.objects.all()
        books['experienceCandidate'] = ExperienceCandidate.objects.all()
        books['userList'] = ProxyUser.objects.all()
        books['typeOfEmployment'] = TypeOfEmployment.objects.all()
        books['reasonForDismissal'] = ReasonForDismissal.objects.all()
        books['priority'] = Priority.objects.all()
        books['candidateStatus'] = CandidateStatus.objects.all()
        books['gradingCategory'] = GradingCategory.objects.all()
        serializer = BooksSerializer(books)

        return Response(serializer.data)


class PkcBooksView(APIView):
    """
    Возвращает все book для сайта PKC24.ru

    """
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        books = {}
        books['gender'] = Gender.objects.filter(in_archive=0)
        books['city'] = City.objects.filter(in_archive=0)
        books['personStatus'] = PersonStatus.objects.filter(in_archive=0)
        books['position'] = Position.objects.filter(in_archive=0)
        books['whereFindOut'] = WhereFindOut.objects.filter(in_archive=0)
        books['experienceCandidate'] = ExperienceCandidate.objects.filter(in_archive=0)
        serializer = BooksSerializer(books)

        return Response(serializer.data)


class CountryModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Страны
    """
    permission_classes = [IsMemberGroup]
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class CityModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Города
    """
    permission_classes = [IsMemberGroup]
    queryset = City.objects.all()
    serializer_class = CitySerializer


class GenderModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Пол
    """
    permission_classes = [IsMemberGroup]
    queryset = Gender.objects.all()
    serializer_class = GenderSerializer


class CitizenshipModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Гражданство
    """
    permission_classes = [IsMemberGroup]
    queryset = Citizenship.objects.all()
    serializer_class = CitizenshipSerializer


class MaritalStatusModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Состояние в браке
    """
    permission_classes = [IsMemberGroup]
    queryset = MaritalStatus.objects.all()
    serializer_class = MaritalStatusSerializer


class RelationDegreeModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает остояние в браке
    """
    permission_classes = [IsMemberGroup]
    queryset = RelationDegree.objects.all()
    serializer_class = RelationDegreeSerializer


class EducationModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Вид образования
    """
    permission_classes = [IsMemberGroup]
    queryset = Education.objects.all()
    serializer_class = EducationSerializer


class PersonStatusModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Статус персоны
    """
    permission_classes = [IsMemberGroup]
    queryset = PersonStatus.objects.all()
    serializer_class = PersonStatusSerializer


class CandidateStatusModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Статус кандидата
    """
    permission_classes = [IsMemberGroup]
    queryset = CandidateStatus.objects.all()
    serializer_class = CandidateStatusSerializer


class PositionModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Должность
    """
    permission_classes = [IsMemberGroup]
    queryset = Position.objects.all()
    serializer_class = PositionSerializer


class BushModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Название куста
    """
    permission_classes = [IsMemberGroup]
    queryset = Bush.objects.all()
    serializer_class = BushSerializer


class OfficeModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Отделения
    """
    permission_classes = [IsMemberGroup]
    queryset = Office.objects.all()
    serializer_class = OfficeSerializer



class OfficeListModelView(ListPermissionViewSetsPR):
    """
    Возвращает Список Отделений
    """

    permission_classes = [IsMemberGroup]
    queryset = Office.objects.all()
    pagination_class = None
    serializer_class = OfficeListSerializer


class DepartmentModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Отделы
    """
    permission_classes = [IsMemberGroup]
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer


class ReasonForOpeningModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    НЕРАБОЧАЯ модель и вычистить базу и front
    """
    permission_classes = [IsMemberGroup]
    queryset = ReasonForOpening.objects.all()
    serializer_class = ReasonForOpeningSerializer


class VacancyStatusModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Статус вакансии
    """
    permission_classes = [IsMemberGroup]
    queryset = VacancyStatus.objects.all()
    serializer_class = VacancyStatusSerializer


class WhereFindOutModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Откуда узнали о_ вакансии
    """
    permission_classes = [IsMemberGroup]
    queryset = WhereFindOut.objects.all()
    serializer_class = WhereFindOutSerializer


class ExperienceCandidateModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Опыт кандидата
    """
    permission_classes = [IsMemberGroup]
    queryset = ExperienceCandidate.objects.all()
    serializer_class = ExperienceCandidateSerializer


class TypeOfEmploymentModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
    Возвращает Вид занятости
    """
    permission_classes = [IsMemberGroup]
    queryset = TypeOfEmployment.objects.all()
    serializer_class = TypeOfEmploymentSerializer


class ReasonForDismissalModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    ''' Возвращает Вид занятости'''
    permission_classes = [IsMemberGroup]
    queryset = ReasonForDismissal.objects.all()
    serializer_class = ReasonForDismissalSerializer


class PriorityModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    ''' Возвращает Вакансия Приоритет'''

    permission_classes = [IsMemberGroup]
    queryset = Priority.objects.all()
    serializer_class = PrioritySerializer


class GradingCategoryModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    ''' Возвращает справочник грейдирования'''

    permission_classes = [IsMemberGroup]
    queryset = GradingCategory.objects.all()
    serializer_class = GradingCategorySerializer
