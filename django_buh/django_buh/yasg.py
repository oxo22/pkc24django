from django.urls import path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from first.permissions import IsMemberGroup

schema_view = get_schema_view(
    openapi.Info(
        title='Django Buh',
        default_version='v1',
        description='Описание доки API',
        license=openapi.License(name='BSD License'),
        filter=True,
    ),
    public=True,
    permission_classes=[IsMemberGroup],
)

urlpatterns = [
    # path('swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
