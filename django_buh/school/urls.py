from django.urls import path
from rest_framework.routers import DefaultRouter

from school.views.bot import BotQuestionModelView, BotQuestionCountView, BotQuestionCategoriesModelView, \
    BotQuestionCountReportView, BotQuestionCountReportUsersView
from school.views.library import LibraryModelView, BookUploadView, LibraryBookCategoryModelView
from school.views.tests import TestingSchoolModelView, PersonTestingSchoolModelView, \
    ReportPersonTestingSchoolModelView, BookPersonTestingSchoolModelView

router = DefaultRouter()

router.register(r'new_employee_test', TestingSchoolModelView, basename='new_employee_test')
router.register(r'person_testing_school', PersonTestingSchoolModelView, basename='person_testing_school')
router.register(r'report_person_testing_school', ReportPersonTestingSchoolModelView,
                basename='report_person_testing_school')
router.register(r'book_person_testing_school', BookPersonTestingSchoolModelView, basename='book_person_testing_school')
router.register(r'bot_questions_school', BotQuestionModelView, basename='bot_questions_school')
router.register(r'bot_questions_categories', BotQuestionCategoriesModelView, basename='bot_questions_categories')
router.register(r'bot_questions_count', BotQuestionCountView, basename='bot_questions_count')
router.register(r'bot_questions_count_report', BotQuestionCountReportView, basename='bot_questions_count_report')
router.register(r'bot_questions_count_report_users', BotQuestionCountReportUsersView,
                basename='bot_questions_count_report_users')
router.register('library', LibraryModelView, basename='library')
# router.register(r'library', LibraryModelView, basename='library_books')
router.register(r'library_book_category', LibraryBookCategoryModelView, basename='library_book_category')

#
urlpatterns = [
    # path('library/', LibraryModelView.as_view(), name='library'),
    path('library/file_upload', BookUploadView.as_view(), name='file_upload')

]

urlpatterns += router.urls
