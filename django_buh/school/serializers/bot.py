from rest_framework import serializers
from school.models.bot import BotQuestionAnswer, BotQuestionCount, Category


class BotQuestionCategoriesModelViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'descr']


class BotQuestionSerializer(serializers.ModelSerializer):
    category = BotQuestionCategoriesModelViewSerializer(read_only=True)

    class Meta:
        model = BotQuestionAnswer
        fields = ['id', 'question', 'answer', 'category', 'resource']


class BotQuestionCountSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField(write_only=True, allow_null=True)
    question_id = serializers.IntegerField(write_only=True, allow_null=True)

    class Meta:
        model = BotQuestionCount
        fields = ['id', 'question', 'question_id', 'user_id']


class BotQuestionCountReportViewSerializer(serializers.Serializer):
    user__id = serializers.IntegerField()
    question__descr = serializers.CharField()
    category = serializers.CharField()
    count = serializers.IntegerField()


class BotQuestionCountReportUsersViewSerializer(serializers.Serializer):
        user__id = serializers.IntegerField()
        last_name = serializers.CharField()
        first_name = serializers.CharField()
        middle_name = serializers.CharField()
