import os

from rest_framework import serializers

from school.models import Book, BookCategory
from school.transliteration import transliterate


class LibraryBookCategoryModelViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = BookCategory
        fields = ['id', 'descr', 'dir_name']

    def create(self, validated_data):
        dir_name  = transliterate(validated_data['descr'])
        validated_data.setdefault('dir_name',dir_name)
        book_category= BookCategory.objects.create(**validated_data)
        os.makedirs('school_library/{}'.format(dir_name), exist_ok=True)
        return book_category


class LibraryBookModelViewSerializer(serializers.ModelSerializer):
    category_id = serializers.IntegerField(write_only=True)

    category = LibraryBookCategoryModelViewSerializer(read_only=True)



    class Meta:
        model = Book
        fields = ['category', 'name', 'descr', 'id', 'category_id', 'file', 'cover']

