from rest_framework import serializers

from first.serializer import UserSerializer
from school.models.tests import TestingSchoolQuestions, TestingSchool, TestingSchoolAnswers, \
    PersonTestingSchool


class SectionReferenceSerializer(serializers.ModelSerializer):

    class Meta:
        model = TestingSchool
        fields = ['id', 'descr']


class TestingSchoolSerializer(serializers.ModelSerializer):
    section_reference = SectionReferenceSerializer(read_only=True)
    test_duration = serializers.CharField(read_only=True)

    class Meta:
        model = TestingSchool
        fields = ['id', 'descr', 'section_reference', 'test_duration']


class TestingSchoolAnswersSerializer(serializers.ModelSerializer):

    class Meta:
        model = TestingSchoolAnswers
        fields = ['id', 'descr', 'in_true']


class TestingSchoolQuestionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = TestingSchoolQuestions
        fields = ['id', 'descr', 'testing']


class TestingQuestionSerializer(serializers.ModelSerializer):

    testing = TestingSchoolSerializer(read_only=True)
    testing_school_answers_testing_school_questions = TestingSchoolAnswersSerializer(read_only=True, many=True)
    test_duration = serializers.IntegerField(read_only=True)

    class Meta:
        model = TestingSchoolQuestions
        fields = '__all__'


class PersonTestingSchoolSerializer(serializers.ModelSerializer):

    user = UserSerializer(read_only=True)

    user_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = PersonTestingSchool
        fields = [
            'id',
            'user',
            'user_id',
            'questions',
            'answers',
            'is_right',
            'test_hash',
        ]


class ReportPersonTestingSchoolSerializer(serializers.Serializer):
    in_date = serializers.DateField(format="%d.%m.%Y")
    questions__testing__descr = serializers.CharField()
    test_hash = serializers.CharField()
    is_right = serializers.CharField()
    last_name = serializers.CharField()
    first_name = serializers.CharField()
    middle_name = serializers.CharField()
    count = serializers.IntegerField()


class BookPersonTestingSchoolSerializer(serializers.Serializer):

    user__id = serializers.IntegerField()
    last_name = serializers.CharField()
    first_name = serializers.CharField()
    middle_name = serializers.CharField()

