from django.contrib import admin
from school.models.bot import Category, BotQuestionAnswer
from school.models.tests import TestingSchool, TestingSchoolQuestions, TestingSchoolAnswers, \
    PersonTestingSchool


class TestingSchoolAnswersFilter(admin.ModelAdmin):
    list_display = (
        'descr',
        'questions',
        'get_testing',
        'in_true'
    )
    list_filter = (
        'questions__testing__descr',
    )
    search_fields = (
        'descr',
        'questions__descr',
    )

    def get_testing(self, obj):
        return obj.questions.testing.descr

    get_testing.admin_order_field = 'testing'  # Allows column order sorting
    get_testing.short_description = 'Наименование теста'  # Renames column head

#
# class PersonTestingSchoolFilter(admin.ModelAdmin):
#     list_display = (
#         'get_full_name',
#         'questions',
#         'get_answers',
#         'get_answers_status',
#     )
#     list_filter = (
#         'questions__testing__descr',
#     )
#     search_fields = (
#         'person__last_name',
#         'person__first_name',
#         'person__middle_name',
#         'questions__descr',
#     )
#
#     def get_full_name(self, obj):
#         return obj.person.last_name + ' ' + obj.person.first_name + ' ' + obj.person.middle_name
#
#     get_full_name.admin_order_field = 'person'  # Allows column order sorting
#     get_full_name.short_description = 'Наименование теста'  # Renames column head
#
#     def get_answers(self, obj):
#         return obj.answers.descr
#
#     get_answers.admin_order_field = 'answers'  # Allows column order sorting
#     get_answers.short_description = 'Ответ теста'  # Renames column head
#
#     def get_answers_status(self, obj):
#         return obj.answers.in_true
#
#     get_answers_status.admin_order_field = 'answers'  # Allows column order sorting
#     get_answers_status.short_description = 'Статус ответ теста'  # Renames column head


class BotQuestionAnswerFilter(admin.ModelAdmin):
    list_display = (
        'question',
        'category'
    )
    list_filter = (
        'category__descr',
    )


admin.site.register(TestingSchoolAnswers, TestingSchoolAnswersFilter)
admin.site.register(PersonTestingSchool)
# admin.site.register(PersonTestingSchool, PersonTestingSchoolFilter)
admin.site.register(TestingSchool)
admin.site.register(TestingSchoolQuestions)
admin.site.register(Category)
admin.site.register(BotQuestionAnswer, BotQuestionAnswerFilter)

