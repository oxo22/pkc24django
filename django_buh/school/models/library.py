from django.db import models
from first.basemodels import Common


class BookCategory(Common):
    id = models.AutoField(primary_key=True)

    descr = models.CharField(max_length=255,
                             verbose_name='Категория книг',
                             blank=False,
                             help_text='Категория книг',
                             unique=True)
    dir_name = models.CharField(max_length=255,
                             verbose_name='Имя папки',
                             blank=True,
                             help_text='Имя папки',
                             unique=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_category_books'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Book(Common):
    id = models.AutoField(primary_key=True)

    name = models.CharField(max_length=255,
                             verbose_name='Наименование книги',
                             blank=False,
                             help_text='Наименование книги')

    descr = models.CharField(max_length=1000,
                             verbose_name='Описание книги',
                             blank=False,
                             help_text='Описание книги')

    file = models.CharField(max_length=255,
                             verbose_name='Имя файла книги',
                             blank=False,
                             help_text='Имя файла книги')

    cover = models.CharField(max_length=255,
                             verbose_name='Имя файла обложки книги',
                             blank=False,
                             help_text='Имя файла обложки книги')
    category = models.ForeignKey(BookCategory,
                                 on_delete=models.PROTECT,
                                 related_name='book_school_category',
                                 null=True,
                                 verbose_name='Категория книг',
                                 blank=True,
                                 help_text='Ссылка на таблицу book_category_books'
                                 )

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'doc_book'
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'

