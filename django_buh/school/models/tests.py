from django.db import models
from book.models import SectionReference
from django_buh import settings
from first.basemodels import Common


class TestingSchool(Common):
    id = models.AutoField(primary_key=True)

    descr = models.CharField(max_length=255,
                             verbose_name='Наименование теста',
                             blank=False,
                             help_text='Наименование теста',
                             unique=True)

    section_reference = models.ForeignKey(SectionReference,
                                          on_delete=models.PROTECT,
                                          related_name='testing_school_testing_school_section_reference',
                                          null=True,
                                          verbose_name='Справочник разделов тестов',
                                          help_text='Ссылка на таблицу book_testing')

    test_duration = models.IntegerField(verbose_name='Продолжительность теста в минутах',
                                        help_text='Продолжительность теста в минутах',
                                        null=True)

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'doc_testing'
        verbose_name = 'Тесты пользователей'
        verbose_name_plural = 'Тесты пользователей'


class TestingSchoolQuestions(Common):

    id = models.AutoField(primary_key=True)

    descr = models.CharField(max_length=255,
                             verbose_name='Вопросы теста',
                             blank=False,
                             help_text='Вопросы теста')

    testing = models.ForeignKey(TestingSchool,
                                on_delete=models.CASCADE,
                                related_name='testing_school_questions_testing_school_testing',
                                null=True,
                                verbose_name='Вопросы к тесту',
                                help_text='Ссылка на таблицу doc_testing')

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'doc_testing_questions'
        verbose_name = 'Вопросы теста'
        verbose_name_plural = 'Вопросы теста'


class TestingSchoolAnswers(Common):

    id = models.AutoField(primary_key=True)

    descr = models.CharField(max_length=255,
                             verbose_name='Ответы теста',
                             blank=False,
                             help_text='Ответы теста')

    questions = models.ForeignKey(TestingSchoolQuestions,
                                  on_delete=models.CASCADE,
                                  related_name='testing_school_answers_testing_school_questions',
                                  null=True,
                                  verbose_name='Вопросы к тесту',
                                  help_text='Ссылка на таблицу doc_testing_questions')

    in_true = models.BooleanField(default=False, verbose_name='Статус ответа', blank=True)

    def __str__(self):
        return 'id: %s : %s - %s - %s' % (
            self.id, self.questions, self.questions.testing, self.in_true)

    class Meta:
        db_table = 'doc_testing_answers'
        verbose_name = 'Ответы теста'
        verbose_name_plural = 'Ответы теста'


class PersonTestingSchool(Common):

    id = models.AutoField(primary_key=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             related_name='person_testing_school_testing_school_user',
                             null=True,
                             verbose_name='Кто сдавал тест',
                             help_text='Ссылка на таблицу doc_first_user')

    questions = models.ForeignKey(TestingSchoolQuestions,
                                  on_delete=models.CASCADE,
                                  related_name='person_testing_school_testing_school_questions',
                                  null=True,
                                  verbose_name='Вопросы к тесту',
                                  help_text='Ссылка на таблицу doc_testing_questions')

    answers = models.ManyToManyField(TestingSchoolAnswers,
                                     related_name='person_testing_school_testing_school_answers',
                                     verbose_name='Ответ теста',
                                     help_text='Ссылка на таблицу doc_testing_answers')
    in_date = models.DateField(auto_now_add=True, editable=False, help_text='Дата прохождения теста')
    is_right = models.BooleanField(default=False, verbose_name='Статус ответа на вопрос')

    test_hash = models.CharField(max_length=40, help_text='Уникальность теста')



    def __str__(self):
        return 'id: %s : - %s - %s' % (
            self.id, self.questions, self.answers)

    class Meta:
        db_table = 'doc_person_testing'
        verbose_name = 'Результаты теста'
        verbose_name_plural = 'Результаты теста'
        unique_together = (('user', 'questions', 'test_hash'),)
