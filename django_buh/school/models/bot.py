from django.db import models
from django_buh import settings
from first.basemodels import Common


class Category(Common):
    id = models.AutoField(primary_key=True)

    descr = models.CharField(max_length=255,
                             verbose_name='Категория вопросов Бота',
                             blank=False,
                             help_text='Категория вопросов')

    def __str__(self):
        return self.descr

    class Meta:
        db_table = 'book_school_bot_category'
        verbose_name = 'Категория вопросов Бота'
        verbose_name_plural = 'Категории вопросов Бота'


class BotQuestionAnswer(Common):
    id = models.AutoField(primary_key=True)

    question = models.CharField(max_length=255,
                                verbose_name='Вопрос',
                                blank=False,
                                help_text='Вопрос')

    answer = models.TextField(verbose_name='Ответ',
                              blank=False,
                              help_text='Ответ')

    resource = models.CharField(max_length=255,
                                verbose_name='Ссылка на ресурс',
                                help_text='Ссылка на ресурс',
                                blank=True,
                                null=True)

    category = models.ForeignKey(Category,
                                 on_delete=models.CASCADE,
                                 related_name='bot_question_answer_school_category',
                                 null=True,
                                 verbose_name='Вопросы Бота',
                                 help_text='Ссылка на таблицу book_school_bot_category')

    def __str__(self):
        return '{} - {}'.format(self.category, self.question)

    class Meta:
        db_table = 'doc_school_bot_question_answer'
        verbose_name = 'Вопрос Бота'
        verbose_name_plural = 'Вопросы Бота'


class BotQuestionCount(Common):
    id = models.AutoField(primary_key=True)

    question = models.ForeignKey(BotQuestionAnswer,
                                 on_delete=models.CASCADE,
                                 related_name='bot_question_count_school_bot_question',
                                 null=True,
                                 verbose_name='Вопрос',
                                 help_text='Ссылка на таблицу doc_school_bot_question')

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             related_name='bot_question_count_school_user',
                             null=True,
                             blank='True',
                             verbose_name='Пользователь системы.',
                             help_text='Ссылка на таблицу auth_user')

    def __str__(self):
        return '{} : {} - {}'.format(self.id, self.user, self.question)

    class Meta:
        db_table = 'doc_school_bot_question_count'
        verbose_name = 'Просмотры Вопроса Бота'
        verbose_name_plural = 'Просмотры Вопросов Бота'
