import os
import shutil

from django.contrib.sites import requests
from django.db.models import Q
from rest_framework import status
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from first.classes import CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR
from school.models import Book, BookCategory
from school.serializers.library import LibraryBookCategoryModelViewSerializer, LibraryBookModelViewSerializer
from school.transliteration import transliterate


class BookUploadView(APIView):
    """
        Загрузка файлов книги и обложки
    """

    parser_classes = (MultiPartParser,)
    permission_classes = [IsAuthenticated]

    @staticmethod
    def post(request):
        file = request.data['file']
        cover = request.data['cover']
        category = request.data['category']

        print(type(file))
        print(type(cover))

        book_filename = transliterate(file.name)
        book_name = '.'.join(book_filename.split(sep='.')[0:-1])
        cover_filename = book_name + '.png'
        print(book_filename)
        print(cover_filename)

        try:
            tmp_book = 'tmp' + book_filename
            dst_book = 'school_library/{}/{}'.format(category, book_filename)

            tmp_cover = 'tmp' + cover_filename
            dst_cover = 'school_library/{}/{}'.format(category, cover_filename)

            with open(tmp_book, 'wb+') as file_handler:
                for line in file.chunks():
                    file_handler.write(line)

            shutil.copy(tmp_book, dst_book)
            os.remove(tmp_book)

            with open(tmp_cover, 'wb+') as file_handler:
                for line in cover.chunks():
                    file_handler.write(line)

            shutil.copy(tmp_cover, dst_cover)
            os.remove(tmp_cover)

        except:
            return Response(status=500)

        return Response(status=201, data=[book_filename, cover_filename])


class LibraryModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
           Клас модели книг
    """
    permission_classes = [IsAuthenticated]
    serializer_class = LibraryBookModelViewSerializer

    def get_queryset(self):
        queryset = Book.objects.all()
        data = self.request.GET.get('data')
        category = self.request.GET.get('category')

        if category:
            queryset = queryset.filter(category_id=category)

        if data:
            dataList = data.split()
            for elem in dataList:
                queryset = queryset.filter(
                    Q(name__icontains=elem) |
                    Q(descr__icontains=elem)
                )
        return queryset

    def destroy(self, request, *args, **kwargs):
        obj = self.get_object()
        category = obj.category.dir_name
        filename = obj.file
        cover = obj.cover


        try:
            os.remove('school_library/{}/{}'.format(category, filename))
            os.remove('school_library/{}/{}'.format(category, cover))
        except :
            pass

        self.perform_destroy(obj)
        return Response(status=status.HTTP_204_NO_CONTENT)


class LibraryBookCategoryModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
           Клас модели категории книг
       """
    permission_classes = [IsAuthenticated]
    queryset = BookCategory.objects.all()
    serializer_class = LibraryBookCategoryModelViewSerializer




