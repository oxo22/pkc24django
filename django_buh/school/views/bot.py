from datetime import datetime

from django.db.models import Q, Count, F, Value, CharField
from rest_framework.permissions import IsAuthenticated

from first.classes import ListPermissionViewSetsPR, CreateUpdateDestroyPR
from first.permissions import IsMemberGroup

from school.models.bot import BotQuestionAnswer, BotQuestionCount, Category
from school.serializers.bot import BotQuestionSerializer, BotQuestionCountSerializer, \
    BotQuestionCategoriesModelViewSerializer, BotQuestionCountReportViewSerializer, \
    BotQuestionCountReportUsersViewSerializer


class BotQuestionModelView(ListPermissionViewSetsPR):
    """
                  Класс отображения вопросов боту

                  - принимает параметры:
                                              search -  поисковый запрос          (type str)
                                              category - категория вопросов       (type str)

                  Возвращает список результатов                                   (type list)
    """
    permission_classes = [IsAuthenticated]
    pagination_class = None
    serializer_class = BotQuestionSerializer

    def get_queryset(self):

        queryset = BotQuestionAnswer.objects.all()
        search = self.request.GET.get('search')
        category = self.request.GET.get('category')

        if category != 'null':
            queryset = queryset.filter(category_id=category)

        if search:
            for word in search.split():
                queryset = queryset.filter(Q(question__icontains=word) |
                                           Q(answer__icontains=word))
            return queryset

        return queryset


class BotQuestionCountView(ListPermissionViewSetsPR, CreateUpdateDestroyPR):
    """
                  Класс отображения ПОДСЧИТАННЫХ вопросов боту

                  Возвращает список результатов                                   (type list)
    """
    permission_classes = [IsAuthenticated]
    pagination_class = None
    serializer_class = BotQuestionCountSerializer
    queryset = BotQuestionCount.objects.all()


class BotQuestionCategoriesModelView(ListPermissionViewSetsPR):
    """
                  Класс отображения КАТЕГОРИЙ вопросов боту
                  Возвращает список результатов                                   (type list)
    """
    permission_classes = [IsAuthenticated]
    pagination_class = None
    serializer_class = BotQuestionCategoriesModelViewSerializer
    queryset = Category.objects.all()


class BotQuestionCountReportView(ListPermissionViewSetsPR):
    """
              Класс отображения отчета по вопросам боту
              - принимает параметры:
                                          start_date -  дата, американский формат   (type date)
                                          stop_date - дата, американский формат     (type date)
                                          user_id - id пользователя                (type int)

              Возвращает список результатов                                         (type list)
    """
    permission_classes = [IsMemberGroup]
    pagination_class = None
    serializer_class = BotQuestionCountReportViewSerializer

    def get_queryset(self):
        start_date = self.request.GET.get('start_date')
        stop_date = self.request.GET.get('stop_date')
        user_id = self.request.GET.get('user_id')
        queryset = BotQuestionCount.objects.all()

        if start_date and stop_date:
            queryset = queryset.filter(create_date_time__range=[
                    datetime.strptime(start_date + ' 00:00', '%Y-%m-%d %H:%M'),
                    datetime.strptime(stop_date + ' 23:59', '%Y-%m-%d %H:%M')
                ])

        if user_id and user_id != 'null' and user_id != 'undefined':
            queryset = queryset.filter(user__id=user_id)

        queryset = queryset.values('user__id',
                                   question__descr=F('question__question'),
                                   category=F('question__category__descr'),
                                   ) \
            .annotate(count=Count('user')).order_by('-count')

        return queryset


class BotQuestionCountReportUsersView(ListPermissionViewSetsPR):
    """
                 Класс отображения ПОЛЬЗОВАТЕЛЕЙ  задававших вопросы боту

                 Возвращает список результатов              (type list)

                 Возвращаемый писок состоит из данных персоны, если она есть, или данных юзера,
                 если персоны нет
    """
    permission_classes = [IsMemberGroup]
    serializer_class = BotQuestionCountReportUsersViewSerializer

    def get_queryset(self):
        list_person = BotQuestionCount.objects.values(
            'user__id',
            last_name=F('user__person__last_name'),
            first_name=F('user__person__first_name'),
            middle_name=F('user__person__middle_name')
        ).filter(user__person__isnull=False).distinct()
        list_user = BotQuestionCount.objects.values(
            'user__id',
            last_name=F('user__last_name'),
            first_name=F('user__first_name'),
            middle_name=Value(' ', output_field=CharField())
        ).filter(user__person__isnull=True).distinct()

        return list_person.union(list_user)
