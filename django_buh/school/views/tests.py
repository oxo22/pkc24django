from django.db.models import Count, F, Value, CharField
from rest_framework.permissions import IsAuthenticated

from first.classes import ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR
from first.permissions import IsMemberGroup
from school.models.tests import PersonTestingSchool
from school.serializers.tests import TestingSchoolQuestions, TestingQuestionSerializer,\
    PersonTestingSchoolSerializer, ReportPersonTestingSchoolSerializer, BookPersonTestingSchoolSerializer


class TestingSchoolModelView(ListPermissionViewSetsPR):
    permission_classes = [IsAuthenticated]
    pagination_class = None
    serializer_class = TestingQuestionSerializer

    def get_queryset(self):
        new_employee_test = self.request.GET.get('new_employee_test')
        queryset = TestingSchoolQuestions.objects.filter(testing__descr=new_employee_test)

        return queryset


class PersonTestingSchoolModelView(ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR):
    permission_classes = [IsAuthenticated]
    pagination_class = None
    serializer_class = PersonTestingSchoolSerializer

    def get_queryset(self):
        queryset = PersonTestingSchool.objects.all()

        return queryset


class BookPersonTestingSchoolModelView(ListPermissionViewSetsPR):
    permission_classes = [IsAuthenticated]
    serializer_class = BookPersonTestingSchoolSerializer

    def get_queryset(self):
        list_person = PersonTestingSchool.objects.values(
            'user__id',
            last_name=F('user__person__last_name'),
            first_name=F('user__person__first_name'),
            middle_name=F('user__person__middle_name')
        ).filter(user__person__isnull=False).distinct()
        list_user = PersonTestingSchool.objects.values(
            'user__id',
            last_name=F('user__last_name'),
            first_name=F('user__first_name'),
            middle_name=Value(' ', output_field=CharField())
        ).filter(user__person__isnull=True).distinct()

        return list_person.union(list_user)


class ReportPersonTestingSchoolModelView(ListPermissionViewSetsPR, CreateRetrieveUpdateDestroyPR):
    permission_classes = [IsMemberGroup]
    serializer_class = ReportPersonTestingSchoolSerializer

    def get_queryset(self):
        user_id = self.request.GET.get('user_id')
        list_person = PersonTestingSchool.objects.values(
            'in_date',
            'questions__testing__descr',
            'is_right',
            'test_hash',
            last_name=F('user__person__last_name'),
            first_name=F('user__person__first_name'),
            middle_name=F('user__person__middle_name'),
        ).filter(user__id=user_id).filter(user__person__isnull=False).annotate(count=Count('is_right')) \
            .order_by('test_hash', 'is_right')
        list_user = PersonTestingSchool.objects.values(
            'in_date',
            'questions__testing__descr',
            'is_right',
            'test_hash',
            last_name=F('user__last_name'),
            first_name=F('user__first_name'),
            middle_name=Value(' ', output_field=CharField()),
        ).filter(user__id=user_id).filter(user__person__isnull=True).annotate(count=Count('is_right')) \
            .order_by('test_hash', 'is_right')

        return list_person.union(list_user)
