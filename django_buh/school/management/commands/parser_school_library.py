import os
import cv2

from django.core.management import BaseCommand

from django_buh.settings import SCHOOL_LIBRARY_ROOT
from pdfrw import PdfWriter, PdfReader
from pdf2image import convert_from_bytes

class Command(BaseCommand):
    help = 'Считывает все данные с библиотеки в формате pdf и записывает 1 страницу в jpg'

    def handle(self, *args, **options):
        def list_dir(data):
            """
                Выбераем список названий пакок и исключаем файл gitignore
            """
            res = [elem for elem in data if elem != '.gitignore']

            return res

        def list_file(data):
            """
                Выбераем список названий файлов в каждой папке
            """
            for elem in data:
                path_root = SCHOOL_LIBRARY_ROOT + '/' + elem
                path = os.listdir(path_root)
                for element in path:
                    try:
                        root = path_root + '/' + element
                        read = PdfReader(root)
                        writer = PdfWriter()
                        try:
                            writer.addpage(read.pages[0])
                        except:
                            writer.addpage(read)
                        writer.write(root + '1.pdf')
                        root_pages = root + '1.pdf'
                        pages = convert_from_bytes(open(root + '1.pdf', 'rb').read())

                        for page in pages:
                            page.save(root[:-4] + '.png', 'PNG')

                        os.remove(root_pages)

                    except:
                        print(path_root + '/' + element)
        path = SCHOOL_LIBRARY_ROOT
        data_dir = os.listdir(path)
        data_list = list_dir(data_dir)
        data = list_file(data_list)