from django.db.models import Count, CharField, Value, IntegerField

from first.classes import ListPermissionViewSetsPR
from first.permissions import IsMemberGroup
from frames.models.vacancies import Vacancies
from reports.serializers.reports_vacancies import OpenClosedSerializer


class OpenClosedModelView(ListPermissionViewSetsPR):
    """
            Класс отображения количество вакансий!

            - принимает параметры:
                                        start_date -  дата, американский формат   (type date)
                                        stop_date - дата, американский формат     (type date)

            Возвращает список результатов                                         (type list)
    """

    permission_classes = [IsMemberGroup]
    pagination_class = None
    serializer_class = OpenClosedSerializer

    def get_queryset(self):
        start_date = self.request.GET.get('start_date')
        stop_date = self.request.GET.get('stop_date')

        object_list_opening = Vacancies.objects.values('office__item').filter(
            opening_date__range=[
                start_date,
                stop_date
            ]
        ).annotate(
            Count('office')
        ).annotate(
            status=Value('opening', output_field=CharField())
        ).order_by('office')

        result_opening = Vacancies.objects.values('office__item').annotate(
            office__count=Value(0, output_field=IntegerField()),
            status=Value('opening', output_field=CharField())
        ).exclude(office__item__in=object_list_opening.values('office__item'))

        object_list_closing = Vacancies.objects.values('office__item').filter(
            closing_date__range=[
                start_date,
                stop_date
            ]
        ).annotate(
            Count('office')
        ).annotate(
            status=Value('closing', output_field=CharField())
        ).order_by('office')

        result_closing = Vacancies.objects.values('office__item').annotate(
            office__count=Value(0, output_field=IntegerField()),
            status=Value('closing', output_field=CharField())
        ).exclude(office__item__in=object_list_closing.values('office__item'))

        return result_closing.union(object_list_closing, object_list_opening, result_opening)
