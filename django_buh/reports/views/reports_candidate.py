from django.db.models import  Q
from django.db.models import Count, CharField, Value
from django.db.models import F
from first.classes import ListPermissionViewSetsPR
from first.models import ProxyUser
from first.permissions import IsMemberGroup
from frames.models.person import ProposalPerson
from reports.serializers.reports_candidate import ProposalSerializer
from frames.models.person import Person
from reports.serializers.reports_candidate import AcceptedFiredCandidateSerializer, ReasonForDismissaleSerializer, \
    SliceOfKnowledgeSerializer


class AcceptedFiredCandidateModelView(ListPermissionViewSetsPR):
    """
            Класс отображения количество вакансий!

            - принимает параметры:
                                        start_date -  дата, американский формат   (type date)
                                        stop_date - дата, американский формат     (type date)

            Возвращает список результатов                                         (type list)
    """

    permission_classes = [IsMemberGroup]
    pagination_class = None
    serializer_class = AcceptedFiredCandidateSerializer

    def get_queryset(self):
        start_date = self.request.GET.get('start_date')
        stop_date = self.request.GET.get('stop_date')

        queryset = Person.objects.all()
        object_list = queryset

        accepted = object_list.values('in_archive').annotate(
            status=Value('приняты', output_field=CharField())
        ).filter(haired_date__range=[
            start_date,
            stop_date
        ]).annotate(Count('in_archive')).order_by('in_archive')

        fired = object_list.values('in_archive').annotate(
            status=Value('уволены', output_field=CharField())
        ).filter(fired_date__range=[
            start_date,
            stop_date
        ]).annotate(Count('in_archive')).order_by('in_archive')

        return accepted.union(fired)


class ReasonForDismissalModelView(ListPermissionViewSetsPR):
    """
            Класс отображения причин увольнения!

            - принимает параметры:
                                        start_date -  дата, американский формат   (type date)
                                        stop_date - дата, американский формат     (type date)

            Возвращает список результатов                                         (type list)
    """

    permission_classes = [IsMemberGroup]
    pagination_class = None
    serializer_class = ReasonForDismissaleSerializer

    def get_queryset(self):
        start_date = self.request.GET.get('start_date')
        stop_date = self.request.GET.get('stop_date')
        otd = self.request.GET.get('otd')

        queryset = Person.objects.all()
        object_list = queryset

        if otd == "true":
            fired = object_list.values(descr=F('position__descr')) \
                .annotate(count=Count('reason_for_dismissal')) \
                .filter(fired_date__range=[start_date, stop_date]) \
                .filter(person_status_id=3) \
                .order_by('position__descr')
        else:
            fired = object_list.values(descr=F('reason_for_dismissal__descr')) \
                .annotate(count=Count('reason_for_dismissal')) \
                .filter(fired_date__range=[start_date, stop_date]) \
                .filter(person_status_id=3) \
                .order_by('reason_for_dismissal__descr')

        return fired


class ProposalModelView(ListPermissionViewSetsPR):
    """
              Класс отображения кандидатов от сети

              - принимает параметры:
                                          start_date -  дата, американский формат   (type date)
                                          stop_date - дата, американский формат     (type date)
                                          office - id отделения                     (type int)
                                          data - фамилия имя отчество пригласившего (type str)
                                          city - id города                          (type int)

              Возвращает список результатов                                         (type list)
    """
    permission_classes = [IsMemberGroup]
    serializer_class = ProposalSerializer

    def get_queryset(self):
        start_date = self.request.GET.get('start_date')
        stop_date = self.request.GET.get('stop_date')
        data = self.request.GET.get('data')
        office = self.request.GET.get('office')
        city = self.request.GET.get('city')

        res = ProposalPerson.objects.values(
            'proposal_person__id',
            'proposal_person__first_name',
            'proposal_person__last_name',
            'proposal_person__middle_name',
            'office_id',
            'office__item',
            'office__city__descr',
            'office__city__id',
        ).annotate(Count('candidate'))

        if start_date and stop_date:
            res = res.filter(create_date_time__range=[start_date, stop_date])

        if office:
            res = res.filter(office=office)

        if city:
            res = res.filter(office__city=city)

        if data:
            data_list = data.split()
            for elem in data_list:
                res = res.filter(
                    Q(proposal_person__last_name__icontains=elem) |
                    Q(proposal_person__first_name__icontains=elem) |
                    Q(proposal_person__middle_name__icontains=elem)
                )
        return res


class SliceOfKnowledgeModelView(ListPermissionViewSetsPR):
    permission_classes = [IsMemberGroup]

    serializer_class = SliceOfKnowledgeSerializer

    def get_queryset(self):
        queryset = ProxyUser.objects.all()

        return queryset
