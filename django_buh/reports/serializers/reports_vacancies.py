from rest_framework import serializers


class OpenClosedSerializer(serializers.Serializer):
    office__item = serializers.CharField()
    status = serializers.CharField()
    office__count = serializers.IntegerField()
