from abc import ABC

from rest_framework import serializers

from book.models import Office
from frames.models.person import ProposalPerson
from book.serializers.books import CandidateStatusSerializer
from first.models import ProxyUser
from frames.models.person import Person, CandidateState
from school.models.tests import PersonTestingSchool, TestingSchoolQuestions


class AcceptedFiredCandidateSerializer(serializers.Serializer):
    status = serializers.CharField()
    in_archive__count = serializers.IntegerField()


class ReasonForDismissaleSerializer(serializers.Serializer):
    descr = serializers.CharField()
    count = serializers.IntegerField()


class ProposalOfficeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Office
        fields = '__all__'


class ProposalSerializer(serializers.ModelSerializer):
    candidate__count = serializers.IntegerField()
    proposal_person__id = serializers.IntegerField()
    proposal_person__first_name = serializers.CharField()
    proposal_person__last_name = serializers.CharField()
    proposal_person__middle_name = serializers.CharField()
    office_id = serializers.IntegerField()
    office__item = serializers.CharField()
    office__city__descr = serializers.CharField()
    office__city__id = serializers.IntegerField()

    class Meta:
        model = ProposalPerson
        fields = [
            'candidate__count',
            'proposal_person__id',
            'proposal_person__first_name',
            'proposal_person__last_name',
            'proposal_person__middle_name',
            'office_id',
            'office__city__descr',
            'office__city__id',
            'office__item', ]


class IsSliceOfKnowledgeCandidateStateSerializer(serializers.ListSerializer):
    """
        Фильтр юзеров по полю person, возвращает тех у кого поле person не null
    """

    def to_representation(self, data):
        # print(data)
        data = data.filter()[:1]
        return super().to_representation(data)


class SliceOfKnowledgeCandidateStateSerializer(serializers.ModelSerializer):
    candidate_status = CandidateStatusSerializer(read_only=True)

    class Meta:
        model = CandidateState
        fields = [
            'candidate_status'
        ]
        list_serializer_class = IsSliceOfKnowledgeCandidateStateSerializer


class SliceOfKnowledgePersonSerializer(serializers.ModelSerializer):
    candidate_state_frames_person = SliceOfKnowledgeCandidateStateSerializer(read_only=True, many=True)

    class Meta:
        model = Person
        fields = [
            'last_name',
            'first_name',
            'middle_name',
            'candidate_state_frames_person',
        ]


class Questions(serializers.ModelSerializer):
    class Meta:
        model = TestingSchoolQuestions
        fields = ['id', 'descr']


class SliceOfKnowledgePersonTestingSchool(serializers.ModelSerializer):

    class Meta:
        model = PersonTestingSchool
        fields = [
            'id',
            'questions',
            'answers',
        ]


class SliceOfKnowledgeSerializer(serializers.ModelSerializer):
    person_testing_school_testing_school_user = SliceOfKnowledgePersonTestingSchool(read_only=True, many=True)
    person = SliceOfKnowledgePersonSerializer(read_only=True)

    class Meta:
        model = ProxyUser
        fields = ['id', 'person', 'person_testing_school_testing_school_user']
