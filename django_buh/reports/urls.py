from rest_framework.routers import DefaultRouter
from reports.views.reports_candidate import AcceptedFiredCandidateModelView, ReasonForDismissalModelView, \
 ProposalModelView, SliceOfKnowledgeModelView
from reports.views.reports_vacancies import OpenClosedModelView

router = DefaultRouter()

router.register(r'open_closed_vacancies', OpenClosedModelView, basename='open_closed_vacancies')
router.register(r'accepted_fired_candidate', AcceptedFiredCandidateModelView, basename='accepted_fired_candidate')
router.register(r'reason_for_dismissal', ReasonForDismissalModelView, basename='reason_for_dismissal')
router.register(r'proposal', ProposalModelView, basename='proposal')
router.register(r'slice_of_knowledge', SliceOfKnowledgeModelView, basename='slice_of_knowledge')

urlpatterns = router.urls
