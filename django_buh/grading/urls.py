from django.urls import path, include
from rest_framework.routers import DefaultRouter

from grading.views.views import GradingPersonsModelView, GradingPersonsStaffListView

router = DefaultRouter()

router.register(r'person_grading', GradingPersonsModelView, basename='person_grading')
router.register(r'person_grading_staff_list', GradingPersonsStaffListView, basename='person_grading_staff_list')

urlpatterns = [
    # path('token/', MyTokenObtainPairView.as_view(), name='token_create'),
]


urlpatterns += router.urls