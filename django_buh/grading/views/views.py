from django.shortcuts import render

# Create your views here.
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from first.classes import CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR
from first.permissions import IsMemberGroup
from frames.models.person import Person
from grading.models.grading import Grading
from grading.serializers.serializer import GradingSerializer, PersonsGradingSerializer


class GradingPersonsModelView(CreateRetrieveUpdateDestroyPR, ListPermissionViewSetsPR):
    """
        Класс отображения всех данных Грейдирование
    """
    permission_classes = [IsMemberGroup]
    queryset = Grading.objects.all()
    serializer_class = GradingSerializer


class GradingPersonsStaffListView(ListPermissionViewSetsPR):
    """
        Класс отображения списка персон сотрудников

    """

    permission_classes = [IsAuthenticated]
    queryset = Person.objects.filter(person_status=2)
    serializer_class = PersonsGradingSerializer