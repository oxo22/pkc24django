from rest_framework import serializers

from book.serializers.books import GradingCategorySerializer, OfficeSerializer, PositionSerializer
from frames.models.person import Person
from grading.models.grading import Grading




class PersonsGradingSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='get_full_name')

    class Meta:
        model = Person
        fields = [
            'id',
            'last_name',
            'first_name',
            'middle_name',
            'full_name',
            'haired_date',
        ]

class GradingSerializer(serializers.ModelSerializer):

    person = PersonsGradingSerializer(read_only=True)
    office = OfficeSerializer(read_only=True)
    position = PositionSerializer(read_only=True)
    category = GradingCategorySerializer(read_only=True)

    person_id = serializers.IntegerField(write_only=True)
    office_id = serializers.IntegerField(write_only=True)
    position_id = serializers.IntegerField(write_only=True)
    category_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Grading
        fields = ('__all__')