from django.db import models

from book.models import Office, Position, GradingCategory
from first.basemodels import Common
from frames.models.person import Person


class Grading(Common):
    id = models.AutoField(primary_key=True)
    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE,
                               related_name='grading_grading_person',
                               blank=False,
                               verbose_name='Персона',
                               help_text='Ссылка на таблицу doc_person')
    office = models.ForeignKey(Office,
                               on_delete=models.CASCADE,
                               related_name='grading_grading_book_office',
                               blank=False,
                               verbose_name='Отделения',
                               help_text='Ссылка на таблицу book_office')
    position = models.ForeignKey(Position,
                                 on_delete=models.CASCADE,
                                 related_name='grading_grading_position',
                                 blank=False,
                                 verbose_name='должность',
                                 help_text='Ссылка на таблицу book_position')
    term_of_work = models.DecimalField(max_digits=19,
                                       decimal_places=2,
                                       blank=True,
                                       null=True,
                                       verbose_name='Срок работы в ПКЦ',
                                       help_text='Срок работы в ПКЦ')
    category = models.ForeignKey(GradingCategory,
                                 on_delete=models.CASCADE,
                                 related_name='grading_grading_category',
                                 blank=False,
                                 verbose_name='должность',
                                 help_text='Ссылка на таблицу book_grading_category')
    month_of_attestation = models.DateField(blank=True,
                                            null=True,
                                            verbose_name='дата атестации',
                                            help_text='дата атестации')
    loyalty = models.IntegerField(verbose_name='лояльность',
                                  blank=True,
                                  null=True,
                                  help_text='лояльность')
    individual_contribution = models.IntegerField(verbose_name='индивидуальный вклад',
                                                  blank=False,
                                                  help_text='индивидуальный вклад')
    training = models.IntegerField(verbose_name='обучение',
                                   blank=True,
                                   null=True,
                                   help_text='обучение')
    mystery_shopper = models.DecimalField(max_digits=19,
                                          decimal_places=2,
                                          verbose_name='тайный покупатель',
                                          blank=True,
                                          null=True,
                                          help_text='тайный покупатель')
    financial_contribution = models.IntegerField(verbose_name='финансовый вклад',
                                                 blank=True,
                                                 null=True,
                                                 help_text='финансовый вклад')
    awards = models.IntegerField(verbose_name='награды',
                                 blank=True,
                                 null=True,
                                 help_text='награды')
    test_result = models.DecimalField(max_digits=19,
                                      decimal_places=2,
                                      verbose_name='результат тестирования',
                                      blank=True,
                                      null=True,
                                      help_text='результат тестирования')

    class Meta:
        db_table = 'doc_grading'
        verbose_name = 'Грейдирование'
        verbose_name_plural = 'Грейдирование'
